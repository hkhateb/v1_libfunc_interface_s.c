//#include <qsound.h>
#include <signal.h>
#include <ctype.h>
#include <setjmp.h>
#include "../_precomp.h"
#include "sound.h"
#include "../public/public.h"
#include "../camera/camera.h"
#include "../finger/finger.h"


/*
#include "../main.h"
#include "../form1.h"
#include "main_ht.h"
#include "../public/public.h"
#include "../note/init_direction.h"
#include "../camera/camera.h"
#include "../note/finger_opt.h"
*/
int splay_overtime_count=0;
sigjmp_buf menu_key_loop; //env-variable structure
sigjmp_buf menu_sound_loop; //env-variable structure


int bjmusic=0;
extern int iTimerInterval;
extern int itsTimer;
extern int successflag;
SOUNDINFO soundinfo;

/*--------------------------------------------------------------------------*
@Function          :InitVolume - Initialize audio device
@Include      	:"sound.h"
@Description		:void
@Return value	: Failure FALSE; Success TRUE	
*---------------------------------------------------------------------------*/
int InitVolume(SOUNDINFO *sound_info)
{
	memset(&soundinfo,0,sizeof(soundinfo));
	soundinfo=*sound_info;
	return TRUE;
}

/*--------------------------------------------------------------------------*
@Function			: setvolume - Volume control
@Include 		     	: "sound.h"
@Description			: percent：volume （0-99）；
@Return Value		: Success TRUE, Failure FALSE.
*---------------------------------------------------------------------------*/
int SetVolume(int percent)
{
	int fd = 0;
	int vol,maxyl=80;

	if ((percent<0)||(percent>100)) 
	{
		perror("percent error");
		return FALSE;
	}

	if ((fd = open("/dev/mixer", O_RDWR)) == -1) 
	{
		perror("open mixer error");
		return FALSE;
	}
	vol=percent*maxyl/100;
	vol |= vol << 8;
	ioctl(fd, MIXER_WRITE(0), &vol);
	close(fd);
	return TRUE;
}

int fileExist(char* fileName)
{
  struct stat info;
  int ret = 0;
  if(!fileName)return ret;
  ret = stat(fileName, &info); //if ret == 0 file exist return true
  return !ret;
}

//play audio file background
/*void Sound(char * command)
{
	char  playcommand[256];
	if(soundinfo.sound_kq !=1||command==NULL||strlen(playcommand)>=200)		return;
	//Stop_BJ_Music();
	memset(playcommand,0,sizeof(playcommand));
	
	sprintf(playcommand,"splay %s &",command);
	system(playcommand);
}*/

pthread_mutex_t * libfun_finger_print_mutex = NULL;

void Sound(char * soundfile)
{

	static pthread_mutex_t sound_mutex = PTHREAD_MUTEX_INITIALIZER; 
	char * fileName;
	char * mp3StrPointer;
	char cacheFileName[50];
	char tempBuff[60];
	int nameLen;
	int mLockRes = -1;

	if(soundfile && fileExist(soundfile))
	{
		fileName = strrchr(soundfile, '/');
		if(!fileName)fileName = soundfile;
		else fileName = fileName+1; // without /
		if(fileName && fileName[0] != '\0')
		{
			//printf("PlaySound mp3 %s %d fileName=%s\n",__FUNCTION__,__LINE__,fileName);
			mp3StrPointer = strstr(fileName,".mp3");
			if(mp3StrPointer)
			{
				cacheFileName[0] = '\0';
				nameLen = mp3StrPointer - (fileName);
				if(nameLen+1 > 50) nameLen = 49;
				snprintf(tempBuff,nameLen+1,"%s",fileName);
				snprintf(cacheFileName,(nameLen+1) - 50,"mp3/%s.decoded",tempBuff);
				//printf("PlaySound mp3 %s %d cacheFileName=%s\n",__FUNCTION__,__LINE__,cacheFileName);
				//decode the mp3 file and save a decoded file
				//lock fpu to fast the decoded
				if(!fileExist(cacheFileName))
				{
					//printf("PlaySound perform decoded mp3 %s %d\n",__FUNCTION__,__LINE__);
					
					pthread_mutex_lock(&sound_mutex); // banned multi threads to decode file the same time
					if(libfun_finger_print_mutex)
						mLockRes = pthread_mutex_trylock(libfun_finger_print_mutex);
					mp3_decoder(soundfile,"/tmp/DecodedMp3");
					if(libfun_finger_print_mutex)
					{
						if(!mLockRes)pthread_mutex_unlock (libfun_finger_print_mutex);
					}

					//unlock the fpu
     					system("./play /tmp/DecodedMp3 &");
					snprintf(tempBuff,60,"cp /tmp/DecodedMp3 %s &",cacheFileName);
					//printf("PlaySound %s %d command = %s\n",__FUNCTION__,__LINE__,mWData->title);
					system(tempBuff);
					pthread_mutex_unlock(&sound_mutex);
				}
				else
				{
					//printf("PlaySound history decoded mp3 %s %d\n",__FUNCTION__,__LINE__);
					snprintf(tempBuff,60,"./play %s &",cacheFileName);
					system(tempBuff);
				}	
			}
			else
			{
				//printf("PlaySound %s %d\n",__FUNCTION__,__LINE__);
				snprintf(tempBuff,60,"./play %s &",soundfile);
				system(tempBuff);
			}
		}
	}
}

void overtime_menu_key(int signo) 
{
	printf("overtime_menu_key %d\n",signo);
	siglongjmp(menu_key_loop,1);//jump  位置
}

//menu prompt voice
void MenuKey(char *command)
{
	char  playcommand[256];
	struct sigaction action;

	cut(command);
	if(soundinfo.sound_key!=1||command==NULL||strlen(playcommand)>200) 	return ;
	Stop_BJ_Music();
	memset(playcommand,0,sizeof(playcommand));
/*	strcpy(playcommand,"splay /etc/music/");
	strcat(playcommand,command);
	strcat(playcommand,".mp3 ");
*/
	sprintf(playcommand,"splay %s",command);
	action.sa_handler=overtime_menu_key;
	sigemptyset(&action.sa_mask);
	sigaction(SIGALRM,&action,NULL);
	alarm(15);
	if(sigsetjmp(menu_key_loop,1)==0)//记录位置
		system(playcommand);
	else {
		soundinfo.sound_key=0;
		soundinfo.sound_kq=0;
		soundinfo.sound_menu=0;	
		splay_overtime_count=1;
	}
	alarm(0);
}

void overtime_menu_sound(int signo) 
{
	printf("overtime_menu_sound %d\n",signo);
	siglongjmp(menu_sound_loop,1);
}

//operation prompt voice
void MenuSound(char *command)
{
	char  playcommand[256];
	struct sigaction action;

	if(soundinfo.sound_menu!=1||command==NULL||strlen(playcommand)>200)	return ;

	Stop_BJ_Music();
	memset(playcommand,0,sizeof(playcommand));
/*	strcpy(playcommand,"splay /etc/music/");
	strcat(playcommand,command);
	strcat(playcommand,".mp3 ");
*/
	sprintf(playcommand,"splay %s",command);
	action.sa_handler=overtime_menu_sound;
	sigemptyset(&action.sa_mask);
	sigaction(SIGALRM,&action,NULL);
	alarm(15);
	if(sigsetjmp(menu_sound_loop,1)==0)
		system(playcommand);
	else {
		soundinfo.sound_key=0;
		soundinfo.sound_kq=0;
		soundinfo.sound_menu=0;	
		splay_overtime_count=1;
	}
	alarm(0);
}

void LinkSound(char *command)
{
	char  playcommand[256];
	
	cut(command);
	if(command==NULL||strlen(command)>200)	return;

	Stop_BJ_Music();
	memset(playcommand,0,sizeof(command));
	sprintf(playcommand,"splay %s &",command);
/*	strcpy(playcommand,"splay ./resfile/music/");
	strcat(playcommand,command);
	strcat(playcommand,".mp3 &");
*/
	system(playcommand);
}

void sound_ok_music(char *bh,char *ka)           //考勤成功后根据策略播放声音。
{
	char music[512],playcommand[512],bhcommand[512],namecommand[64],okcommand[64];
	char *p,tmpbuf[64];
	int i=0;   //编号不能超过7为，超过则截断。
	char *path="/etc/music/";
	static char bhbuf[512],flag=0;

	if(soundinfo.sound_kq != 1)		return;
	memset(okcommand,0,64);
	memset(music,0,512);
	memset(namecommand,0,64);
	memset(bhcommand,0,512);
	memset(playcommand,0,512);
	Stop_BJ_Music();
//	sprintf(okcommand," %s%s.mp3",path,q_direction[curdirection].sound);   //ok

	if(bh&&(fp_enable==1&&camera_enable==0)&&(!flag))
	{
		memset(bhbuf,0,sizeof(bhbuf));
		if(bh)		strcpy(bhbuf,bh);
		flag=1;
		return ;
	}
	if(flag==1)
	{	
		flag=0;
		p=bhbuf;
	}
	else
		p=bh;

	if(p==NULL)		return;
	else
		while(*p)                      
		{
			memset(tmpbuf,0,64);
			if(!isdigit(*p))
			{
				p++;
				continue;
			}
			sprintf(tmpbuf," %sn%c.mp3",path,*p);
			strcat(bhcommand,tmpbuf);                       //报编号
			i++;   
			p++;
			if(i>7)	break;                   //编号最多报7个声音
		}
	strcpy(playcommand,"splay ");
	if(ka)		strcat(playcommand," /etc/music/ka.mp3");  //播发卡声音
//	if(!door_opend) 
//		strcat(playcommand," /etc/music/ec.mp3");
//	else 
		if(soundinfo.sound_ok==0)
			strcat(playcommand,okcommand);
		else if(soundinfo.sound_ok==1)
			strcat(playcommand,bhcommand);
		else if(soundinfo.sound_ok==2)
		{
			strcat(playcommand,bhcommand);
			strcat(playcommand,okcommand);  
		}   

	if(!(fp_enable==1&&camera_enable==0))
			strcat(playcommand," &");
	system(playcommand);
}
//play background music ./music/* ./music/q.mp3

int Start_BJ_Music(char *str)
{
	char command[128],*mplayer="/weds/kq42/mplayer";
	char *dir,*file;
	int count1=0,count2=0,count=0;	

	if(str==NULL||bjmusic>0)	return FALSE;

	dir=getbeginname(str);
	if(!dir_countfile_all(dir))	return FALSE;

	file=getendname(str);
	memset(command,0,sizeof(command));
	if(strcmp(file,"*")==0){
		count1=countfile_suffix(dir,".mp3");
		count2=countfile_suffix(dir,".MP3");
		if(count1>0)		count=count1;
		if(count2>0)		count+=count1;
	}
	if(count||strstr(file,".mp3")||strstr(file,".MP3"))
	{
		count=1;
		sprintf(command,"splay %s -r >/dev/null &",str);

	}
	else
	{
		if((access(mplayer,F_OK)==0)&&(access(mplayer,X_OK)==0))
			sprintf(command,"%s -ac mad -framedrop -loop 0 -fs %s >/dev/null &",mplayer,str);
	}

//	printf("%s\n",command);
	if(system(command)==0)
	{
	//		form1->setFocus();
		if(count>0)		bjmusic=1;	//play mp3 file
		else bjmusic=2;			//play video file
		return TRUE;
	}

	return FALSE;
}
//stop playing background music
int Stop_BJ_Music()
{
	FILE   *stream; 
	char command[128],buff[512],*p;
	pid_t splaypid;
	int pidnum;

	if(bjmusic==0)	return FALSE;
	memset(command,0,sizeof(command));
	if(bjmusic==2)
		sprintf(command,"ps -e|grep mplayer");
	else sprintf(command,"ps -e |grep splay");
	stream=popen(command,"r");
	if(stream!=NULL)
	{
		memset(buff,0,sizeof(buff));
		while(fgets(buff,512,stream))
		{
			p=strtok(buff," ");
			if(p==NULL)		break ;
			if(strlen(p)==0)	break;
			if((pidnum=atoi(p))==0)	break;
			splaypid=(pid_t)pidnum;
			kill(splaypid,SIGKILL);
		}
		pclose(stream);
	}
	bjmusic=0;
	return TRUE;
/*
	if(bjmusic==2)
	{ 
		form1->hide();
		form1->show();
	}
	bjmusic=0;

	getdirection();
	if(curdirection>=0)
	{
		sprintf(buff,"%s %s %s",kstr[105],q_direction[curdirection].text,kstr[106]);
		writets("ok3",buff,NULL);
	}
	else 
	{
		sprintf(buff,"%s",kstr[163]);
		writets("ok3",buff,NULL);
	}
*/
}

void cleanup(mpg123_handle *mh)
{
	mpg123_close(mh);
	mpg123_delete(mh);
	mpg123_exit();
}

int mp3_decoder(char * inputfile, char * outputfile)
{
	SNDFILE* sndfile = NULL;
	SF_INFO sfinfo;
	mpg123_handle *mh = NULL;
	unsigned char* buffer = NULL;
	size_t buffer_size = 0;
	size_t done = 0;
	int  channels = 0, encoding = 0;
	long rate = 0;
	int  err  = MPG123_OK;
	off_t samples = 0;

	if (!inputfile || !outputfile) return -1;
	//printf( "Input file: %s\n", inputfile);
	//printf( "Output file: %s\n", outputfile);
	
	err = mpg123_init();
	if(err != MPG123_OK || (mh = mpg123_new(NULL, &err)) == NULL)
	{
		fprintf(stderr, "Basic setup goes wrong: %s", mpg123_plain_strerror(err));
		cleanup(mh);
		return -2;
	}

	/* Simple hack to enable floating point output. */
	//if(argc >= 4 && !strcmp(argv[3], "f32")) mpg123_param(mh, MPG123_ADD_FLAGS, MPG123_FORCE_FLOAT, 0.);

	    /* Let mpg123 work with the file, that excludes MPG123_NEED_MORE messages. */
	if(    mpg123_open(mh, inputfile) != MPG123_OK
	    /* Peek into track and get first output format. */
	    || mpg123_getformat(mh, &rate, &channels, &encoding) != MPG123_OK )
	{
		fprintf( stderr, "Trouble with mpg123: %s\n", mpg123_strerror(mh) );
		cleanup(mh);
		return -3;
	}

	if(encoding != MPG123_ENC_SIGNED_16 && encoding != MPG123_ENC_FLOAT_32)
	{ /* Signed 16 is the default output format anyways; it would actually by only different if we forced it.
	     So this check is here just for this explanation. */
		cleanup(mh);
		fprintf(stderr, "Bad encoding: 0x%x!\n", encoding);
		return -4;
	}
	/* Ensure that this output format will not change (it could, when we allow it). */
	mpg123_format_none(mh);
	mpg123_format(mh, rate, channels, encoding);

	bzero(&sfinfo, sizeof(sfinfo) );
	sfinfo.samplerate = rate;
	sfinfo.channels = channels;
	sfinfo.format = SF_FORMAT_WAV|(encoding == MPG123_ENC_SIGNED_16 ? SF_FORMAT_PCM_16 : SF_FORMAT_FLOAT);
	printf("Creating WAV with %i channels and %liHz.\n", channels, rate);

	sndfile = sf_open(outputfile, SFM_WRITE, &sfinfo);
	if(sndfile == NULL){ fprintf(stderr, "Cannot open output file!\n"); cleanup(mh); return -2; }

	/* Buffer could be almost any size here, mpg123_outblock() is just some recommendation.
	   Important, especially for sndfile writing, is that the size is a multiple of sample size. */
	//buffer_size = argc >= 5 ? atol(argv[4]) : mpg123_outblock(mh);
	buffer_size = mpg123_outblock(mh);
	buffer = malloc( buffer_size );

	do
	{
		sf_count_t more_samples;
		err = mpg123_read( mh, buffer, buffer_size, &done );
		more_samples = encoding == MPG123_ENC_SIGNED_16
			? sf_write_short(sndfile, (short*)buffer, done/sizeof(short))
			: sf_write_float(sndfile, (float*)buffer, done/sizeof(float));

		//test line code-need to remove
		//more_samples = done/mpg123_encsize(encoding);
		
		if(more_samples < 0 || more_samples*mpg123_encsize(encoding) != done)
		{
			fprintf(stderr, "Warning: Written number of samples does not match the byte count we got from libmpg123: %li != %li\n", (long)(more_samples*mpg123_encsize(encoding)), (long)done);
		}
		samples += more_samples;
		/* We are not in feeder mode, so MPG123_OK, MPG123_ERR and MPG123_NEW_FORMAT are the only possibilities.
		   We do not handle a new format, MPG123_DONE is the end... so abort on anything not MPG123_OK. */
	} while (err==MPG123_OK);

	if(err != MPG123_DONE)
	fprintf( stderr, "Warning: Decoding ended prematurely because: %s\n",
	         err == MPG123_ERR ? mpg123_strerror(mh) : mpg123_plain_strerror(err) );

	sf_close( sndfile );

	samples /= channels;
	//printf("%li samples written.\n", (long)samples);
	cleanup(mh);
	return 0;
}

static int open_sound_device (int channels, int srate)
{	
	int fd, stereo, fmt ;

	if ((fd = open ("/dev/dsp", O_WRONLY, 0)) == -1 &&
		(fd = open ("/dev/sound/dsp", O_WRONLY, 0)) == -1)
	{	perror ("open_sound_device : open ") ;
		return -1 ;
	} ;

	stereo = 0 ;
	if (ioctl (fd, SNDCTL_DSP_STEREO, &stereo) == -1)
	{ 	/* Fatal error */
		perror ("open_sound_device : stereo ") ;
		return -2 ;
	} ;

	if (ioctl (fd, SNDCTL_DSP_RESET, 0))
	{	perror ("open_sound_device : reset ") ;
		return -3 ;
	} ;

	fmt = CPU_IS_BIG_ENDIAN ? AFMT_S16_BE : AFMT_S16_LE ;
	if (ioctl (fd, SNDCTL_DSP_SETFMT, &fmt) != 0)
	{	perror ("open_sound_device : set format ") ;
		return -4 ;
  	} ;

	if (ioctl (fd, SNDCTL_DSP_CHANNELS, &channels) != 0)
	{	perror ("open_sound_device : channels ") ;
		return -5 ;
	} ;

	if (ioctl (fd, SNDCTL_DSP_SPEED, &srate) != 0)
	{	perror ("open_sound_device : sample rate ") ;
		return -6 ;
	} ;

	if (ioctl (fd, SNDCTL_DSP_SYNC, 0) != 0)
	{	perror ("open_sound_device : sync ") ;
		return -7 ; ;
	} ;

	return 	fd ;
} /* open_sound_device */

int sound_play (char * soundfile)
{	
	static short buffer [BUFFER_LEN] ;
	SNDFILE *sndfile ;
	SF_INFO sfinfo ;
	int	audio_device, readcount, writecount=0, subformat ;

	memset (&sfinfo, 0, sizeof (sfinfo)) ;

	//printf ("Playing %s\n", soundfile) ;
	if (! (sndfile = sf_open (soundfile, SFM_READ, &sfinfo)))
	{	
		puts (sf_strerror (NULL)) ;
		return -1;
	} 

	if (sfinfo.channels < 1 || sfinfo.channels > 2)
	{
		printf ("Error : channels = %d.\n", sfinfo.channels) ;
		return -2;
	} 

	audio_device = open_sound_device (sfinfo.channels, sfinfo.samplerate) ;

	subformat = sfinfo.format & SF_FORMAT_SUBMASK ;

	if (subformat == SF_FORMAT_FLOAT || subformat == SF_FORMAT_DOUBLE)
	{	
		static float float_buffer [BUFFER_LEN] ;
		double	scale ;
		int 	m ;

		sf_command (sndfile, SFC_CALC_SIGNAL_MAX, &scale, sizeof (scale)) ;
		if (scale < 1e-10)
			scale = 1.0 ;
		else
			scale = 32700.0 / scale ;

		while ((readcount = sf_read_float (sndfile, float_buffer, BUFFER_LEN)))
		{	for (m = 0 ; m < readcount ; m++)
					buffer [m] = scale * float_buffer [m] ;
				writecount = write (audio_device, buffer, readcount * sizeof (short)) ;
		} ;
	}
	else
	{	
		while ((readcount = sf_read_short (sndfile, buffer, BUFFER_LEN)))
			writecount = write (audio_device, buffer, readcount * sizeof (short)) ;
	} ;

	if (ioctl (audio_device, SNDCTL_DSP_POST, 0) == -1)
		perror ("ioctl (SNDCTL_DSP_POST) ") ;

	if (ioctl (audio_device, SNDCTL_DSP_SYNC, 0) == -1)
		perror ("ioctl (SNDCTL_DSP_SYNC) ") ;

	close (audio_device) ;

	sf_close (sndfile) ;

	return writecount ;
} /* sound_play */
