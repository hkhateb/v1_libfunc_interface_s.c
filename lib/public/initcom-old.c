#include "initcom.h"
#include "../_precomp.h"
#include "../public/public.h"
#include "../print/printcom.h"

 
TCom serialport1=COM2,serialport2=COM3;
int comovertime=30;
_TData com1value;

/*
//test use
int opencomm(int mode,int baud)
{
	char id=1,i=1;


		if(start_com_io((enum _TCom)i,baud)==0)
		{
			printf("set serialport1 error=%d\n",i);
			return FALSE;
		}
		_set_work_mode((enum _TCom)i,(enum _TWorkMode)mode);
	
		if(_set_address(id) == 0)
		{
			printf("set address error=%d\n",i);
			return FALSE;
		}
		serialport1=(enum _TCom)i;

	return TRUE;
}
*/
/*--------------------------------------------------------------------------*
@Function			:OpenCom - Initialize serial port
@Include      		: "com.h"
@Description			: port��serial port No.
				baudrate : serial band rate
				overtime : overtime  used during communication
				set the serial port
@Return Value		: Success 0 .Failure -1
@Create time			: 2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int OpenCom(IN COMINFO *com_info)
{
	char id;
	int bianhao=1,i=0;
	int baud[]= {2400,4800,9600,19200,38400,57600,115200};

	if(com_info[i].enable==1)
	{
		if(com_info[i].workmode == 1)	//printer  port
		{
			OpenDev(i,baud[com_info[i].baudrate],8,1,'N');	//  initialize port
		}
		else
		{
			set_speed("/dev/ttyS0",baud[com_info[i].baudrate]);
			UnPrintCom();
		}
		printdevinfo.printflag=com_info[i].workmode;	//com1
	}
	

	for(i=1;i<=2;i++)
	{
		if(com_info[i].enable==0)	continue;
		if(start_com_io((enum _TCom)i,baud[com_info[i].baudrate])==0)
		{
			printf("set comport error=%d\n",i);
		}
		_set_work_mode((enum _TCom)i,(enum _TWorkMode)com_info[i].workmode);
	
		bianhao=read_terminalno();
		id=bianhao;
		if(id==0X00||id==0XFF)	id=1;
		if(_set_address(id) == 0)
		{
			printf("set address error=%d\n",id);
		}
		if(i==2)		//485
		{
			serialport2=(enum _TCom)i;
			comovertime=com_info[i].overtime;
		}
		else if(i==1)		//card
			serialport1=(enum _TCom)i;
	}
	return TRUE;

}

/*--------------------------------------------------------------------------*
@Function			: UnCom - close serial port
@Include      		: "com.h"
@Description			: release resource
						
@Return Value		: void		
*---------------------------------------------------------------------------*/
void UnCom(IN int comnum)
{

	switch(comnum){
		case 0:
			UnPrintCom();
		break;
		case 1:
			CloseComPort((enum _TCom)comnum);
		break;
		case 2:
			CloseComPort((enum _TCom)comnum);
		break;
		default:
			UnPrintCom();
			CloseComPort((enum _TCom)1);		
			CloseComPort((enum _TCom)2);
		break;
	}
}


//get com data
int ReadCom1(void)
{
	memset(&com1value,0,sizeof(com1value));
	if (_get_data(serialport1,&com1value)==SUCCESS)
	{
		ComPort_Clear(serialport1);	//clear com1 buffer
		return TRUE;
	} 
	return FALSE;
}


//sett terminal mode
int Set_Machine_Mode(IN int mode)
{		
	_TData answer;
	
	if(mode>255||mode<0)	return FALSE;
	memset(&answer,0,sizeof(answer));
	answer.address=0X01;
	answer.instruction=0X8F;		
	answer.nbytes=0X01;
	answer.itemnum=1;
	answer.user_data[0]=mode;
	if(_put_data(serialport1,&answer)!=SUCCESS)	return FALSE;
	tcdrain(serialport1);
//	ComPort_Clear(serialport1);	//clear com1 buffer
	return TRUE;		
}

//get card serial number
int mifs_request()
{
	static _TData data;

	if (_get_data(serialport1,&data)==SUCCESS)
	{
		if(data.instruction==READMF1CARD)
		{
			return TRUE;
		}
	} 
	return FALSE;
}



/*
//_Adr --data address
this function will get 16bytes data when card pass verification
Sector:sector No.
Block:block No.
Akey:A-key
Bkey:B-key
out
data:
*/
int mifs_read(char *cardsno,char Sector, char Block, unsigned char *Data, char mode,char *key)
{
	_TData data;
	_TData answer;
	char *ptr,buf[3];
	int i=0,j=0;
	struct timeval oldtime,newtime;
	int oldsec;
//printf("key=%s\n",key);
//	if(Sector>15||Sector<0||Block>3||Block<0)	return 1;
	memset(&answer,0,sizeof(answer));
	answer.address=0X01;
	if(key==NULL)
	{
		answer.instruction=0X82;		
		answer.nbytes=2+4;
	}
	else
	{
		answer.instruction=0X80;
		answer.nbytes=14+4-6+1;
	}
	answer.itemnum=1;
	j=gsmString2Bytes((const char*)cardsno,&answer.user_data[i],strlen(cardsno));
	i+=j;
	answer.user_data[i++]=Sector;
	answer.user_data[i++]=Block;
	if(key!=NULL)
		answer.user_data[i++]=mode;
	if(key!=NULL)
	{
		j=0;
//printf("key=%s\n",key);
		j=gsmString2Bytes((const char*)key,&answer.user_data[i],strlen(key));
	}
	if(_put_data(serialport1,&answer)!=SUCCESS)	return FALSE;
//usleep(50000);
	gettimeofday(&oldtime,NULL);
	oldsec=(oldtime.tv_sec % 10000) *1000+oldtime.tv_usec/1000;
	while(1){
		gettimeofday(&newtime,NULL);
		if (abs(((newtime.tv_sec % 10000) *1000+newtime.tv_usec/1000)-oldsec)>300) 
		{
			return FALSE;//there is no card in 200ms
		}
		memset(&data,0,sizeof(data));
		if (_get_data(serialport1,&data)==SUCCESS)	//read data in block
		{
//printf("read instruction:%02X\n",data.instruction);
			if(data.instruction==0X80)
			{
				memcpy(Data,data.user_data,16);
				return TRUE;
			}
		}	 
	}
	return FALSE;			
}
//д��
int mifs_write(char *cardsno,char Sector, char Block, unsigned char *Data, char mode,char *key)
{		
	_TData answer,data;
	char *ptr,buf[3];
	int i=0,j=0;
	struct timeval oldtime,newtime;
	int oldsec;

//	if(strlen((char*)Data)!=16||Sector>15||Sector<0||Block>3||Block<0)	return 1;
	memset(&answer,0,sizeof(answer));
	answer.address=0X01;
	if(key==NULL)
	{
		answer.instruction=0X83;		
		answer.nbytes=18+4;
	}
	else
	{
		answer.instruction=0X81;
		answer.nbytes=30+4-6+1;
	}
	answer.itemnum=1;
	j=gsmString2Bytes((const char*)cardsno,&answer.user_data[i],strlen(cardsno));
	i+=j;
	answer.user_data[i++]=Sector;
	answer.user_data[i++]=Block;
	if(key!=NULL)
		answer.user_data[i++]=mode;

	if(key!=NULL)
	{
		j=0;
		j=gsmString2Bytes((const char*)key,&answer.user_data[i],strlen(key));
		i+=j;
	}

	memcpy(&answer.user_data[i],Data,16);

	if(_put_data(serialport1,&answer)!=SUCCESS)	return FALSE;

	gettimeofday(&oldtime,NULL);
	oldsec=(oldtime.tv_sec % 10000) *1000+oldtime.tv_usec/1000;
	while(1){
		gettimeofday(&newtime,NULL);
		if (abs(((newtime.tv_sec % 10000) *1000+newtime.tv_usec/1000)-oldsec)>300) 
		{
			return FALSE;//there is no card in 200ms
		}
		memset(&data,0,sizeof(data));
		if (_get_data(serialport1,&data)==SUCCESS)	//read data in block
		{
//printf("write instruction:%02X,%02X\n",data.instruction,data.user_data[0]);
			if(data.instruction==0X00&&data.user_data[0]==0X00)
			{
				return TRUE;
			}
		}	 
	}

	return FALSE;		
}
