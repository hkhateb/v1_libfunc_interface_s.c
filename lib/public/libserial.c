#include "libserial.h"

/*--------------------------------------------------------------------------*
@Function		: set_time_out_trigger - Set a timeout trigger value 
@Prototype         : int set_time_out_tirgger(TTimer *timer, fdouble seconds)
@Include     	: "timer.h"
@Description       : seconds: time when overtime
                    timer��pointer of timer 
                    It reset the timer.after a new trigger value is set��
                    start_timer must be called.trigger value must be set before using the timer
                    otherwise the result undefined.Except that it is only used to measure time
@Return Value      : if seconds < 0.0 or timer if NULL��return 0; else return 1
*---------------------------------------------------------------------------*/
int set_time_out_trigger(TTimer *timer, double seconds) {

    if(seconds < 0.0 || timer == NULL)
        return 0;
    timer->begin = timer->end = 0.0;
    timer->trigger_ticks = seconds * PRECISION;
    return 1;
} /* set_time_out_trigger */

/*--------------------------------------------------------------------------*
@Function					: get_time_out_trigger - get the trigger value
@Prototype        : double get_time_out_tirgger(TTimer *timer)
@Include     			: "timer.h"
@Description      : timer��pointer of timer 
                    this function must be called to get the old trigger value each time to reset it 
@Return Value     : if timer is NULL ,return 0.0; otherwise return the old trigger value
*---------------------------------------------------------------------------*/
double get_time_out_trigger(TTimer *timer) {

    return timer == NULL ? 0.0 : timer->trigger_ticks / PRECISION;
} /* get_time_out_trigger */

/*--------------------------------------------------------------------------*
@Function					: stop_timer - close timer
@Prototype        : int stop_timer(TTimer *timer)
@Include     			: "timer.h"
@Description      : timer��pointer of timer
                    timer status should be set as STOP when close timer.
                    different from end_timer��after calling stop_timer��is_time_out always reurn 0;
                    that is to say ,it will never overtime.after stop_timer��when start_timer��start_timer will set the timer'status to RUN automaticly.
@Return Value     : Success 1��Failure 0
*---------------------------------------------------------------------------*/
int stop_timer(TTimer *timer) {

    if(timer == NULL)
        return 0;
    timer->status = STOP;
    return 1;
} /* stop_timer */

/*--------------------------------------------------------------------------*
@Function					: start_timer - start to timing
@Prototype        : int start_timer(TTimer *timer)
@Include     			: "timer.h"
@Description      : timer��pointer to timer 
@Return Value     : Success 1��Failure 0
*---------------------------------------------------------------------------*/
int start_timer(TTimer *timer) {

    struct timeval tv;

    if(timer == NULL)
        return 0;
		gettimeofday(&tv, NULL);
    timer->end = 0.0;
    timer->begin = (double)tv.tv_sec * PRECISION + (double)tv.tv_usec;
    timer->status = RUN;
    return 1;
} /* start_timer */

/*--------------------------------------------------------------------------*
@Function					: end_timer - end timing
@Prototype        : int end_timer(TTimer *timer)
@Include     			: "timer.h"
@Description      : timer��pointer to timer 
                    end timing. when the timer'status is STOP,reurn 0
@Return Value     : Success 1��otherwise 0
*---------------------------------------------------------------------------*/
int end_timer(TTimer *timer) {

    struct timeval tv;

	if(timer == NULL || timer->status == STOP)
        return 0;
	gettimeofday(&tv, NULL);
    timer->end = (double)tv.tv_sec * PRECISION + (double)tv.tv_usec;
    return 1;
} /* end_timer */

/*--------------------------------------------------------------------------*
@Function					: time_used
@Prototype        : double time_used(TTimer *timer)
@Include     			: "timer.h"
@Description      : timer��pointer to timer. 
                    Calculated the time used from the beginning to the end.
                    notice you must first end timing and then call this function
@Return Value     : return time used when success�� otherwise return 0.0
*---------------------------------------------------------------------------*/
double time_used(TTimer *timer) {

    if(timer == NULL || timer->end <= 0.0)
		return 0.0;

    return (timer->end - timer->begin) / PRECISION;
} /* time_used */

/*--------------------------------------------------------------------------*
@Function					: is_time_out - check whether it is overtime
@Prototype        : int is_time_out(TTimer *timer)
@Include     			: "timer.h"
@Description      : timer��pointer to timer 
                    when timer's status is STOP��it return 0,that is to say it
                    will never overtime
@Return Value     : if overtime,return non-zero,else return 0
*---------------------------------------------------------------------------*/
int is_time_out(TTimer *timer) {

    struct timeval tv;
    double clicks;
    gettimeofday(&tv, NULL);
    clicks = (double)tv.tv_sec * PRECISION + (double)tv.tv_usec - timer->begin;
    if(timer == NULL || timer->status == STOP)
        return 0;
    return  clicks >= timer->trigger_ticks || clicks < 0 ? 1 : 0;
} /* is_time_out */


#define TIMEOUT_SEC(buflen,baud) (buflen * 20 / baud + 2)
#define TIMEOUT_USEC 0

#define BUFFER_LEN	1024    /* sendfile() */

static INT32    fd[MAXCOM];
static struct termios termios_old[MAXCOM], termios_new[MAXCOM];
static fd_set   fs_read, fs_write;
static struct timeval tv_timeout;


static void     SetBaudrate (INT32 ComPort, INT32 baudrate);
static void     SetDataBit (INT32 ComPort, INT32 databit);
static INT32    BAUDRATE (INT32 baudrate);
static INT32    SetPortAttr (INT32 ComPort, INT32 baudrate, INT32 databit,
                             const char *stopbit, char parity);
static void     SetStopBit (INT32 ComPort, const char *stopbit);
static void     SetParityCheck (INT32 ComPort, char parity);

INT32 OpenComPort (INT32 ComPort, INT32 baudrate, INT32 databit,
                   const char *stopbit, char parity)
{
    char           *pComPort;
    INT32           retval;
    switch (ComPort) {
    case 0:
        pComPort = "/dev/ttyS0";
        break;
    case 1:
        pComPort = "/dev/ttyS1";
        break;
    case 2:
        pComPort = "/dev/ttyS2";
        break;
    case 3:
        pComPort = "/dev/ttyS3";
        break;
    case 4:
        pComPort = "/dev/ttyS4";
        break;
    case 5:
        pComPort = "/dev/ttyS5";
        break;
    case 6:
        pComPort = "/dev/ttyS6";
        break;
    case 7:
        pComPort = "/dev/ttyS7";
        break;
    default:
        pComPort = "/dev/ttyS0";
        break;
    }

    fd[ComPort] = open (pComPort, O_RDWR |  O_NOCTTY | O_NONBLOCK); //| O_SYNC)
    if (-1 == fd[ComPort]) {
        fprintf (stderr, "cannot open port %s: %s\n", pComPort,strerror(errno));
        return (-1);
    }
    tcgetattr (fd[ComPort], &termios_old[ComPort]); /* save old termios value */
    /* 0 on success, -1 on failure */
    retval = SetPortAttr (ComPort, baudrate, databit, stopbit, parity);
  
    if (-1 == retval) {
        fprintf (stderr, "\nport %s cannot set baudrate at %d: %s\n", pComPort,
                 baudrate, strerror(errno));
    }
    return (retval);
}


void CloseComPort (INT32 ComPort)
{

  if(fd[ComPort] == 0)
    return ;
  tcsetattr (fd[ComPort], TCSADRAIN, &termios_old[ComPort]);
  close (fd[ComPort]);
}

void ComPort_Clear(int ComPort)
{
 if(fd[ComPort] >0)
 tcflush(fd[ComPort],TCIOFLUSH);
}

INT32 ReadComPort (INT32 ComPort, void *data, INT32 datalength)
{
    INT32           retval = 0;

    FD_ZERO (&fs_read);
    FD_SET (fd[ComPort], &fs_read);
    tv_timeout.tv_sec = 0;//TIMEOUT_SEC (datalength, GetBaudrate ());
    tv_timeout.tv_usec = 150000;//TIMEOUT_USEC;

    retval = select (fd[ComPort] + 1, &fs_read, NULL, NULL, &tv_timeout);
    if (retval)
        return (read (fd[ComPort], data, datalength));
    else
        return (-1);
}

int ReadComPort2(INT32 ComPort, void *data, INT32 datalength) {

  int recvbytes, n;
  n = recvbytes = 0;

  if(ioctl(fd[ComPort], FIONREAD, &recvbytes) != 0) {
    perror("ioctl");
    return -1;
  }

  if(recvbytes <= 0 || recvbytes < datalength)
    return -1;
//    printf("I want %d bytes. There are %d bytes available\n",     datalength, recvbytes);


  if(recvbytes >= datalength)
    n = read(fd[ComPort], data, datalength);
  return n;
} /* ReadComPort2 */


INT32 WriteComPort (INT32 ComPort, const UINT8 * data, INT32 datalength)
{
    INT32       retval, len = 0, total_len = 0;
    FD_ZERO (&fs_write);
    FD_SET (fd[ComPort], &fs_write);
    tv_timeout.tv_sec = 0;//TIMEOUT_SEC (datalength, GetBaudrate ());
    tv_timeout.tv_usec = 150000;//TIMEOUT_USEC;
   // tcflush (fd[ComPort], TCOFLUSH);
return write (fd[ComPort],data, datalength );
    for (total_len = 0, len = 0; total_len < datalength;)
      {
        retval = select (fd[ComPort] + 1, NULL, &fs_write, NULL, &tv_timeout);
        if (retval) 
         {
            len = write (fd[ComPort], &data[total_len], datalength - total_len);
            if (len > 0) 
            {
                total_len += len;
                tcflush (fd[ComPort], TCOFLUSH);
            }
        }
        else 
        {
            tcflush (fd[ComPort], TCOFLUSH);
            break;
        }
    }
    return (total_len);
}

static void SetBaudrate (INT32 ComPort, INT32 baudrate)
{
    termios_new[ComPort].c_cflag |= BAUDRATE (baudrate);
}

static void SetDataBit (INT32 ComPort, INT32 databit)
{
    termios_new[ComPort].c_cflag &= ~CSIZE;
    switch (databit) {
    case 8:
        termios_new[ComPort].c_cflag |= CS8;
        break;
    case 7:
        termios_new[ComPort].c_cflag |= CS7;
        break;
    case 6:
        termios_new[ComPort].c_cflag |= CS6;
        break;
    case 5:
        termios_new[ComPort].c_cflag |= CS5;
        break;
    default:
        termios_new[ComPort].c_cflag |= CS8;
        break;
    }
}

static void SetStopBit (INT32 ComPort, const char *stopbit)
{
    if (0 == strcmp (stopbit, "1")) {
        termios_new[ComPort].c_cflag &= ~CSTOPB; /* 1 stop bit */
    }
    else if (0 == strcmp (stopbit, "1.5")) {
        termios_new[ComPort].c_cflag &= ~CSTOPB; /* 1.5 stop bits */
    }
    else if (0 == strcmp (stopbit, "2")) {
        termios_new[ComPort].c_cflag |= CSTOPB;  /* 2 stop bits */
    }
    else {
        termios_new[ComPort].c_cflag &= ~CSTOPB; /* 1 stop bit */
    }
}

static void SetParityCheck (INT32 ComPort, char parity)
{
    switch (parity) {
    case 'N':                  /* no parity check */
        termios_new[ComPort].c_cflag &= ~PARENB;
        break;
    case 'E':                  /* even */
        termios_new[ComPort].c_cflag |= PARENB;
        termios_new[ComPort].c_cflag &= ~PARODD;
        break;
    case 'O':                  /* odd */
        termios_new[ComPort].c_cflag |= PARENB;
        termios_new[ComPort].c_cflag |= ~PARODD;
        break;
    default:                   /* no parity check */
        termios_new[ComPort].c_cflag &= ~PARENB;
        break;
    }
}

static INT32 SetPortAttr (INT32 ComPort, INT32 baudrate,
                          INT32 databit, const char *stopbit, char parity)
{
    bzero (&termios_new[ComPort], sizeof (termios_new));
    cfmakeraw (&termios_new[ComPort]);
    SetBaudrate (ComPort, baudrate);
    termios_new[ComPort].c_cflag |= CLOCAL | CREAD;
    SetDataBit (ComPort, databit);
    SetParityCheck (ComPort, parity);
    SetStopBit (ComPort, stopbit);
    termios_new[ComPort].c_oflag = 0;
    termios_new[ComPort].c_lflag |= 0;
    termios_new[ComPort].c_oflag &= ~OPOST;
    termios_new[ComPort].c_cc[VTIME] = 1;        /* 1/10 second. */
    termios_new[ComPort].c_cc[VMIN] = 1;
    tcflush (fd[ComPort], TCIFLUSH);
    return (tcsetattr (fd[ComPort], TCSANOW, &termios_new[ComPort]));
}

static INT32 BAUDRATE (INT32 baudrate)
{
//printf("baudrate %d\n",baudrate);
    switch (baudrate) {
    case 0:
        return (B0);
    case 50:
        return (B50);
    case 75:
        return (B75);
    case 110:
        return (B110);
    case 134:
        return (B134);
    case 150:
        return (B150);
    case 200:
        return (B200);
    case 300:
        return (B300);
    case 600:
        return (B600);
    case 1200:
        return (B1200);
    case 2400:
        return (B2400);
    case 9600:
        return (B9600);
    case 19200:
        return (B19200);
    case 38400:
        return (B38400);
    case 57600:
        return (B57600);
    case 115200:
        return (B115200);
    default:
        return (B9600);
    }
}


/*--------------------------------------------------------------------------*
@Function					: cal_bcc - XOR Check
@Prototype        : char cal_bcc(void *data, short int nbytes)
@Include     			: "protocol.h"
@Description      : data��pointer to data block
                    nbytes��number of bytes in data
                    Calculation formula��BCC = DATA(0)^DATA(1)^DATA(2)^...^DATA(nbytes - 1)��
@Return Value     : XOR Check value
										if  data=NULL or nbytes<=0 return 0 
*----------------------------------------------------------------------------*/
unsigned char cal_bcc(void *data, short int nbytes) {

    short int i = 0;
    unsigned char bcc = 0, *p = (unsigned char *)data;

    if(data == NULL || nbytes <= 0)
        return 0;

    while(i < nbytes)
        bcc = (unsigned char)(bcc ^ (p[i++] & 0xFF));

    return bcc;
} /* cal_bcc */

/*--------------------------------------------------------------------------*
@Function					: cal_sum  - SUM Check
@Prototype        : char cal_sum(void *data, short int nbytes)
@Include     			: "protocol.h"
@Description      : data��pointer to data block
                    nbytes��number of bytes in data
                    Calculation formula��
                    SUM = LOWBATE(DATA(0)+DATA(1)+DATA(2)+...+DATA(nbytes - 1))��
                    in which LOWBATE means get the lowest bytes of data.
@Return Value     : SUM Check value
                    if  data=NULL or nbytes<=0 return 0 
*----------------------------------------------------------------------------*/
unsigned char cal_sum(void *data, short int nbytes) {

    short int i = 0;
    unsigned char *p = (unsigned char *)data;
    unsigned long int sum = 0UL;

    if(data == NULL || nbytes <= 0)
        return 0;

    while(i < nbytes)
        sum += (p[i++] & 0xFF);
    
    return (unsigned char)(sum & (unsigned long int)0xFF);     /* Only get the minimum bytes */
} /* cal_sum */

/*--------------------------------------------------------------------------*
@Function					: put_byte - send one byte
@Prototype        : TError put_byte(TCom com, unsigned char byte)
@Include     			: "serio.h "
@Description      : byte��
                    send one byte to serial.
                    None byte was send within the specified time means overtime
@Return Value     : if one byte was send within the specified time,return SUCCESS
                    else return FAILURE
*----------------------------------------------------------------------------*/
TError put_byte(TCom com, unsigned char byte) {

    TError error;
    //printf("put_byte %02X\n",byte);
    error = WriteComPort((INT32)com, &byte, 1) == 1 ? SUCCESS : FAILURE;
     //printf("put_byte %02X\n",byte);
    return error;
} /* put_byte */

/*--------------------------------------------------------------------------*
@Function					: get_byte - receive one byte
@Prototype        : TError get_byte(TCom com, unsigned char *byte)
@Include     			: "serio.h"
@Description      : byte��
                    recieve one byte from serial.
                    None byte was received within the specified time means overtime
@Return Value     : if one byte was received within the specified time,return SUCCESS
                    else return FAILURE
*----------------------------------------------------------------------------*/
TError get_byte(TCom com, unsigned char *byte) {

    TError error;
    int r;

    error = ((r = ReadComPort((INT32)com, byte, 1)) == 1) ? SUCCESS : FAILURE;
    return error;
} /* get_byte */
/*--------------------------------------------------------------------------*
@Function					: do_put_data  - send data
@Prototype        : TError do_put_data(TCom com, const void *data, short int nbytes)
@Include     			: "serio.h"
@Description      : data��data will be send
                    nbytes��
                    less than nbytes were send within the specified time means overtime
                    and will return 
@Return Value     : nbytes were send completely return Success��Failure return  FAILURE or TIME_OUT�� 
*----------------------------------------------------------------------------*/
TError do_put_data(TCom com, const unsigned char  *data, short int nbytes) {

    int sended = 0, n;

    assert(data != NULL && nbytes > 0);
    //printf("do_put_data\n");
    while(sended != nbytes) {
       n = WriteComPort((INT32)com, data + sended,
                        nbytes - sended);
          //printf("put_data  %d\n",n);
       if(n == -1)
           return TIME_OUT;
       sended += n;
    }

    return SUCCESS; 
} /* do_put_data */

/*--------------------------------------------------------------------------*
@Function					: do_get_data - receive data
@Prototype        : TError do_get_data(TCom com, void *data, short int nbytes)
@Include     			: "serio.h"
@Description      : data��where to store data received
                    nbytes��number of byte will received
                    less than nbytes were received within the specified time means overtime
                    and will return 
@Return Value     : nbytes were received completely return Success��Failure return  FAILURE or TIME_OUT�� 
*----------------------------------------------------------------------------*/
TError do_get_data(TCom com, void *data, short int nbytes) {

    int received = 0, n;

    assert(data != NULL && nbytes > 0);
    //printf("do_get_data %d\n",nbytes);
    while(received != nbytes) {
       n = ReadComPort((INT32)com, (void *)((char *)data + received),
                       nbytes - received);
       // printf("get %d\n",n);
       if(n == -1)
         return TIME_OUT;
       received += n;
    }
    return SUCCESS;
} /* do_get_data */

/*--------------------------------------------------------------------------*
@Function		: start_com_io - open serial
@Prototype         : int start_com_io(int com, TBaudRate baud_rate,
                                   TWrodLength word_length, TStopBits stop_bits,
                                   TParity parity)
@Include     	: "serio.h"
@Description       : com��serial port .Olny COM1 and COM2 can use at the present time 
                    baud_rate_index��baudrate index defined in chip.h
                    trigger��time needed to receive or send a byte
                    databits,stopbits and parity are set by function automaticly
                    It's work is to open serial port,set timer and communication address
@Return Value     : Success 1 ; failure 0
*----------------------------------------------------------------------------*/
int start_com_io(TCom com, long baud_rate) {

    int wl;
    char sb[4] = {0};
    char od;
    //long int baud;

    switch(_8) {
      case _5: case _6: case _7: case _8:
          wl = _8 + 5;
          break;
      default:
          wl = 8;
    }

    switch(_1) {
      case _1:
          sb[0] = '1';
          break;
      case _1_5:
          sb[0] = '1';
          sb[1] = '.';
          sb[2] = '5';
          break;
      case _2:
          sb[0] = '2';
          break;
      default:
          sb[0] = '1';
          break;
    }

    switch(_none) {
        case _odd:
          od = 'O';
          break;
        case _even:
          od = 'E';
          break;
        case _none:
          od = 'N';
          break;
        default:
          od = 'N';
    }
    //baud = baud_rate_table[baud_rate].baud_rate;
    if(OpenComPort((INT32)com, baud_rate, wl, sb, od) == -1)
        return 0;
    return 1;
} /* start_com_io */

/*--------------------------------------------------------------------------*
@Function					: end_com_io   - close serial port
@Prototype        : void end_com_io(TCom com)
@Include     			: "serio.h"
@Description      : end communication,close serial port and destroy timer
@Return Value     : None
*----------------------------------------------------------------------------*/
void end_com_io(TCom com) {

    CloseComPort((INT32)com);
} /* end_com_io */


/*------------------------------------------------------------------------
 * File description�� 
 * File name - protocol.c 
 *
 * Function list �� 
 *        _set_address                - set communication address
 *        _get_address                - get communication address
 *        _set_work_mode              - set work mode
 *        _get_work_mode              - get work mode
 *        _get_package                - get data packet
 *        _get_data                   - get data
 *        _put_data                   - send data
 *        _make_package               - make packet
 *        _analyze_package            - analyze data packet
 *-----------------------------------------------------------------------*/

/* communication between server and terminal ��source files of terminal */


/* the max length terminal can send or receive */
enum {_MAXBUFLEN = MAXLENGTH};
/*--------------------------------------------------------------------------
 * communication address of itself��used to check whether a packet was send by itself
 *-------------------------------------------------------------------------*/
static char _address;

/*--------------------------------------------------------------------------
 * server address��not used��
 *-------------------------------------------------------------------------*/
/*static char serveraddress;*/

/*--------------------------------------------------------------------------
 * work modes are terminal mode and server mode .start signs are _STY and _STX
 * _begin_putch��start sign used when send
 * _begin_getch��start sign used when receive
 *-------------------------------------------------------------------------*/

unsigned char _begin_putch[MAXCOM], _begin_getch[MAXCOM]; /* default : terminal mode */
static TError _get_package(TCom com,TPackage package, char begin, char end);
static TError _make_package(TPackage package, _TData *data, short int *total);
static int _analyze_package(TPackage package, _TData *data);


/*--------------------------------------------------------------------------*
@Function					: _set_address  - set communication address
@Prototype        : int _set_address(char address)
@Include     			: "terpro.h"
@Description      : address��The communication address used to mark a receiver.
@Return Value     : Success return non-zero;else 0
*----------------------------------------------------------------------------*/
int _set_address(unsigned char address) {

    return _address = (address == _SERVERADDR || address == _BROADCASTADDR ? 0 : address);
} /* _set_address */

/*--------------------------------------------------------------------------*
@Function					: _get_address  - get address
@Prototype        : int _get_address(void)
@Include     			: "terpro.h"
@Description      : get address 
@Return Value     : return address
*----------------------------------------------------------------------------*/
char _get_address(void) {

    return _address;
} /* _get_address */

/*--------------------------------------------------------------------------*
@Function					: _set_work_mode  - set work mode
@Prototype        : void _set_work_mode(TWorkMode work_mode)
@Include     			: "terpro.h"
@Description      : work_mode��it's value is _TERMINAL or _SERVER
@Return Value     : None
*----------------------------------------------------------------------------*/
void _set_work_mode(TCom com,TWorkMode work_mode) {

    switch(work_mode) {
        case _TERMINAL:
            _begin_putch[com] = _STY;
            _begin_getch[com] = _STX;
            break;
        case _SERVER:
            _begin_putch[com] = _STX;
            _begin_getch[com] = _STY;
            break;
        default:
            _begin_putch[com] = _STY; /* default : terminal mode */
            _begin_getch[com] = _STX;
            break;
    }
} /* _set_work_mode */

/*--------------------------------------------------------------------------*
@Function					: _get_work_mode  - get work mode
@Prototype        : TWorkMode _get_work_mode(void)
@Include     			: "terpro.h"
@Description      : get work mode
@Return Value     : current work mode
*----------------------------------------------------------------------------*/
TWorkMode _get_work_mode(TCom com) {

    return (_begin_putch[com] == _STX && _begin_getch[com] == _STY) ? _SERVER : _TERMINAL;
} /* _get_work_mode */

/*--------------------------------------------------------------------------*
@Function					: _get_package  -  receive data packet
@Prototype        : static TError _get_package(TPackage package, char begin, char end)
@Include     			: only functions in this file can use this function
@Description      : package��store data packet
                    begin��start byte of data
                    end��end byte of data
                    receive a packet from serial port��
                    less than specified bytes were received within the specified time means overtime and will return 
@Return Value     : return BEGINERROR when no begin received��
										return NOTMYADDRESS when the address received was not it's��
										return LENGTHERROR when no byte which specified the length received
										return ENDERROR when there was no end received
                    you can refer to��
                 overtime receive specified bytes  return val      remarks
                      Y       Y                    no defined   it is impossible
                      N       Y                    SUCCESS
                                                             		maybe useful data has been received
                      Y       N                    TIME_OUT 		how to deal with it up to hign level function
                
                      N       N                    FAILURE  
                      data is NULL                 FAILURE 
*----------------------------------------------------------------------------*/
static TError _get_package(TCom com,TPackage package, char begin, char end) {

    short int nbytes;
    unsigned char ch = 0, *p;
int i=0;

    if(package == NULL)
       return FAILURE;
    lable:
       p = (unsigned char *)package;

   while(1)
   {
    if(get_byte(com,&ch) != SUCCESS)   /* receive begin */
        return TIME_OUT;   
//printf("\n\n");
//printf("begin=%02X\n",ch);
    if(ch != begin)
        continue;
    if(get_byte(com,&ch) != SUCCESS)   /* according to protocol,this byte is address */
        return TIME_OUT;
//printf("add=%02X\n",ch);
    /* when work in terminal mode,check whether it is the local addr��while work in server mode,do not check */
    if(_get_work_mode(com) == _TERMINAL && ch != _get_address())
	return TIME_OUT;
     else break;
    }
    *p++ = ch;                     /* put addr byte to the first byte of package */
    if(get_byte(com,&ch) != SUCCESS)   /* according to protocol,this byte is length of data */
       return TIME_OUT;
//printf("bytes=%02X\n",ch);
    *p++ = ch;                     /* put data byte to the second byte of package */
    nbytes = (short int)ch + 2;    /* add 2 in order to receive BCC and SUM bits */

    if(do_get_data(com,p, nbytes) == TIME_OUT) /* receive bits */
        return TIME_OUT;
//for(i=0;i<nbytes;i++)
//	printf("%02X ",*p++);
//printf("\n");
    if(get_byte(com,&ch) != SUCCESS)  /* according to protocol,this byte should be end */
        return TIME_OUT;
//printf("end=%02X\n",ch);
    if(ch != end)
     goto lable;
//printf("1111\n");
    return SUCCESS;
} /* _get_package */

/*--------------------------------------------------------------------------*
@Function					: _put_data   -  send data
@Prototype        : TError _put_data(_TData *data)
@Include     			: "terpro.h"
                    data��nbytes=0 means no data��itemnu will be set to 0 automaticly,
                    and ignore use_data
@Return Value    	: Failure return TIME_OUT;else return SUCCESS��
										if data=NULL��return  FAILURE
*----------------------------------------------------------------------------*/
TError _put_data(TCom com,_TData *data) {

    TPackage package;
    short int total;
    unsigned char *ptr;
    int i;

    ptr=(unsigned char *)data;
    //printf("%02X,%02X,%02X,%02X\n",*ptr++,*ptr++,*ptr++,*ptr++);
    if(data ==NULL || data->nbytes > 253)
        return FAILURE;
    _make_package(package, data, &total);
//for(i=0;i<total;i++)
//printf("%02X ",package[i]);
//printf("\n");
    if(put_byte(com,_begin_putch[com]) == TIME_OUT)  /* communication start */
        return TIME_OUT;
    if(do_put_data(com,(unsigned char *)package, total) == TIME_OUT)
        return TIME_OUT;
    if(put_byte(com,_ETX) != SUCCESS)     /* communication end */
        return TIME_OUT;
//printf("send ok\n");
    return SUCCESS;
} /* _put_data */

/*--------------------------------------------------------------------------*
@Function		: _get_data  -  receive data
@Prototype        	: TError _get_data(_TData *data)
@Include     	: "terpro.h"
@Description      	: data��store data
                    after the data has been received,analyse it and assign the parameters 
                    according to the result.
@Return Value     	: Failure return error code��
			If the error is discovered after analysis,return code that stands for anlysis error.others return SUCCESS.if data=NULL return  FAILURE
*----------------------------------------------------------------------------*/
int _get_data(TCom com,_TData *data) {

    TError error;
    TPackage package;

    if(data == NULL)
        return FAILURE;
    if((error = _get_package(com,package, _begin_getch[com], _ETX)) != SUCCESS)
        return error;
    
    return _analyze_package(package, data);
} /* _get_data */

/*--------------------------------------------------------------------------*
@Function					: _make_package - make packete
@Prototype        : static TError _make_package(TPackgae package, 
                  :  _TData *data, short int *total)
@Include     			: only functions in this file can use this function
@Description      : data��nbytes=0 means no data��itemnu will be set to 0 automaticly,
                    and ignore use_data
                    total��return number of total effectual bytes
@Return Value     : always success
                    Notice��it do not check any parameters
*----------------------------------------------------------------------------*/
static TError _make_package(TPackage package, _TData *data, short int *total) {

    unsigned char *p = (unsigned char *)package;

    p[_ADDRESS] = data->address;
    p[_INSTRUCTION] = data->instruction; // the third byte of data is instruction
    if(data->nbytes == 0) {         /* nbytes = 0��means no data */
        p[_ITEMNUM] = data->itemnum = 0;   
        memset(p + _ITEMNUM + 1, 0, _MAXBUFLEN - 4);
        p[_LENGTH] = 2;   
    }
    else {
         p[_ITEMNUM] = data->itemnum;
         memcpy(p + _ITEMNUM + 1, data->user_data, data->nbytes);
         p[_LENGTH] = data->nbytes + 2;
    }

    p[p[_LENGTH] + 2] = cal_bcc(p, p[_LENGTH] + 2);
    p[p[_LENGTH] + 3] = cal_sum(p, p[_LENGTH] + 2);
    /* 2��there is two bytes in header of packet��address and length�� 
     * 2��total bytes of instruction and data 
     * 2��BCC��SUM are total two bytes */
    *total = 2+ 2 + data->nbytes + 2;
    return SUCCESS;
} /* _make_package */

/*--------------------------------------------------------------------------*
@Function					: _analyze_package   -  analyze data packet
@Prototype        : static TError _analyze_package(TPackage package, _TData *data)
@Include     			: only functions in this file can use this function
@Description      : data��
                    first deal with the tail of the packete:
                        1��bcc check��compare with bcc in package
                        2��sum check��compare with sum in package
                    if ok,assign corresponding value to parameters
                    Notice��it do not check any parameters
@Return Value     : return value are followings
                    bcc check ok  sum check ok   return val
                          Y             Y       SUCCESS
                          Y             N       SUMERROR
                          N            	Y       BCCERROR
                          N             N       BCCERROR | SUMERROR
*----------------------------------------------------------------------------*/
static int  _analyze_package(TPackage package, _TData *data) {

    //TError error = SUCCESS;
      int error=SUCCESS;
    unsigned char *p = (unsigned char *)package;

    if(p[p[_LENGTH] + 2] != cal_bcc(p, p[_LENGTH] + 2))
        error |= BCCERROR;
    if(p[p[_LENGTH] + 3] != cal_sum(p, p[_LENGTH] + 2))
        error |= SUMERROR;
    if(error != SUCCESS)
        return error;
    /* assign correspongding value to fields of  data */
    data->instruction = p[_INSTRUCTION];
    data->address = p[_ADDRESS];
    data->nbytes = p[_LENGTH] - 2;
    data->itemnum = p[_ITEMNUM];
    memcpy(data->user_data, p + 4, p[_LENGTH] - 2);
    return SUCCESS;
} /* _analyze_package */
