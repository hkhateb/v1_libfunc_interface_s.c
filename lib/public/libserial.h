#ifndef _LIBSERIAL_H
#define _LIBSERIAL_H 1
#include <sys/time.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <termios.h>            /* tcgetattr, tcsetattr */
#include <stdio.h>              /* perror, printf, puts, fprintf, fputs */
#include <unistd.h>             /* read, write, close */
#include <fcntl.h>              /* open */
#include <sys/signal.h>
#include <sys/types.h>
#include <string.h>             /* bzero, memcpy */
#include <limits.h>             /* CHAR_MAX */
#include <errno.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/ioctl.h>

typedef enum _TStatus {STOP = 0x01, RUN = 0x02} TStatus;
typedef struct _TTimer {
    double begin, end;
    TStatus status;  /* with value STOP or RUN */
    double trigger_ticks; /* Overtime trigger value */
} TTimer;

/* the MAX-number of bytes of User data buffer*/

typedef enum _TWordLength {
    _5,   /* 5 bits data */
    _6,   /* 6 bits data */
    _7,   /* 7 bits data */
    _8    /* 8 bits data */
} TWordLength;

typedef enum _TParity {
    _odd,
    _even,
    _none
} TParity;

typedef enum _TStopBits {
    _1,
    _1_5,
    _2
} TStopBits;




//typedef enum _TStatus {STOP = 0x01, RUN = 0x02} TStatus;
enum {PRECISION = 1000000};

double get_time_out_trigger(TTimer *timer);
int stop_timer(TTimer *timer);
int end_timer(TTimer *timer);
double time_used(TTimer *timer);

enum {_USERDATALEN = 253};
typedef enum _TError {SUCCESS = 0x7F00, TIME_OUT, FAILURE, NOTMYADDRESS,
                      BEGINERROR, ENDERROR, BCCERROR, SUMERROR, LENGTHERROR,
                      UNKNOWNERROR
                     } TError;
typedef enum _TCom {COM1, COM2, COM3, COM4, COM5, COM6, COM7, COM8} TCom;

typedef struct __TData {
    unsigned char address;     /* address of receive��server��/send��terminal�� */
    unsigned char nbytes;      /* the count of data in user_data */
   unsigned char instruction; /* instruction to receive or send */
  unsigned  char itemnum;     /* the count of logic-independance data units */
  unsigned  char user_data[_USERDATALEN + 1]; /* user_data */
} _TData;

int start_com_io(TCom com, long baud_rate);
void end_com_io(TCom com);
void ComPort_Clear(int ComPort);

int _set_address(unsigned char address);
char _get_address(void);
TError _put_data(TCom com,_TData *data);
int _get_data(TCom com,_TData *data);
int set_time_out_trigger(TTimer *timer, double trigger_seconds);
int start_timer(TTimer *timer);
int is_time_out(TTimer *timer);



enum {MAXLENGTH = 259};

/* data packet */
typedef char TPackage[MAXLENGTH]; 

unsigned char cal_bcc(void *data, short int nbytes);
unsigned char cal_sum(void *data, short int nbytes);
TError put_byte(TCom com, unsigned char byte);
TError get_byte(TCom com, unsigned char *byte);
TError do_put_data(TCom com, const unsigned char  *data, short int nbytes);
TError do_get_data(TCom com, void *data, short int nbytes);

typedef int     LONG;
//typedef unsigned short WORD;
//typedef unsigned int DWORD;
//typedef unsigned char BYTE;
typedef char    TCHAR;

typedef int     INT32;
typedef short   INT16;
//typedef char    INT8;
typedef unsigned int UNIT32;
typedef unsigned short UINT16;
typedef unsigned char UINT8;

enum {MAXCOM = 8};
extern unsigned char _begin_putch[MAXCOM], _begin_getch[MAXCOM]; /* default : terminal mode */

/* serial.c */
INT32 OpenComPort (INT32 ComPort, INT32 baudrate, INT32 databit,
                   const char *stopbit, char parity);
void CloseComPort (INT32 ComPort);
INT32 ReadComPort (INT32 ComPort, void *data, INT32 datalength);
INT32 ReadComPort2 (INT32 ComPort, void *data, INT32 datalength);

INT32 WriteComPort (INT32 ComPort, const UINT8 * data, INT32 datalength);
INT32 SendFile (INT32 ComPort, const char *pathname, INT32 echo);



/* 
 *Server call/Terminal response mode is used for serial communication.
 */

/*-------------------------------------*
 * sign        value     description
 * TER_STX	   0xAA   	 call to communicate
 * TER_STY	   0xAB	  	 acknowledge to communicate
 * TER_ETX	   0xA5	  	 communicatin end
 * ADD		          		 receive address
 * LEN		          		 packet size=N+1
 * BCC		          		 Xor-parity 
 * SUM		          		 SUM-parity
 *------------------------------------*/
  /*call start,acknowledge start, communication end*/
enum {_STX = 0xAA, _STY = 0xAB, _ETX = 0xA5};

/*-------------------------------------*
 * Server address ADD��0��Terminal address ADD��1~254��ADD=255 is broadcast address��no response
 * BCC= ADD^LEN^DATA[0]^DATA[1]^��^DATA[N]  	Xor-parity
 * SUM= ADD+LEN+DATA[0]+DATA[1]+��+DATA[N]		SUM-parity��high-bytes are ignored��
 *------------------------------------*/
/* Terminal addresses are between SERVERADD and BROADCASTADD */
enum {_SERVERADDR = 0x00, _BROADCASTADDR = 0xFF};


/* 
 * diagram show the structrue of packet transfered between terminal and server��
 *                                   |��----- packet -----��| 
 * +----------------------------------------------------------------------+
 * |ADD | LEN | INSTRUCTION |ITEMNUM |USER DATA(MAX 253 BYTES)| BCC | SUM |
 * +----------------------------------------------------------------------+
 *            |��-------- 255 bytes��TOTALLENGTH��-----------��|
 */ 

enum {_ADDRESS, _LENGTH, _INSTRUCTION, _ITEMNUM};

/* work mode��terminal or server */
typedef enum _TWorkMode {_TERMINAL, _SERVER} TWorkMode;
void _set_work_mode(TCom com,TWorkMode work_mode) ;
TWorkMode _get_work_mode(TCom com);

#endif
