#include "../public/public.h"
#include "print.h"

struct 
{
	int flag;             //0-transmit,1-print
	int total;               //the total number of packet which should be print
	int curcount;                 //the cur number of packet have print
	FILE *fp;               //file pointer
}printmemory;


int GetFileInfo(char *filename)
{
	struct stat filestat;

	if(printdevinfo.printflag==0)	return FALSE;
	if(ReadComStatus()==FALSE||printmemory.flag!= 0) 
	{
		return FALSE;	
	}
	memset(&printmemory,0,sizeof(printmemory));

	printmemory.fp=fopen(filename,"r");
	if(printmemory.fp==NULL)
	{
		   return FALSE;
	 }
	 printmemory.flag=1;	
	 fstat(fileno(printmemory.fp),&filestat); 
	 printmemory.total=filestat.st_size;

	return TRUE;
}

/*--------------------------------------------------------------------------*
@Function			:PrintFile - print files
@Include			: "print.h"
@Description			: filename: the file that will be print
@Return Value		: Success TREU, Failure FALSE.
@Create time			: 2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int PrintFile(void)
{
	int len=0;
	char  buf[50];  
	int overtime; 
	struct timeval tv1,tv2;

	if(printdevinfo.printflag==0)	return FALSE;
	 if(printdevinfo.fd<=0||printmemory.flag != 1)
	 {
		return FALSE;
	 }

	gettimeofday(&tv1,NULL);
	overtime=(tv1.tv_sec % 10000) *1000+tv1.tv_usec/1000;
	while(1){
		 memset(buf,0,64);
		 if(ReadComStatus()==FALSE)	return FALSE;
		 if(fseek(printmemory.fp,printmemory.curcount,SEEK_SET)<0)
		 {
			printmemory.flag=0;
			return FALSE;			
		 }
		 len=fread(buf, 1, sizeof(buf), printmemory.fp);
	 	 if(WriteComm( (unsigned char*)buf, strlen(buf)) == FALSE)	return FALSE;
		 printmemory.curcount+=len;
		if(printmemory.curcount==printmemory.total)
		{
			printmemory.flag=0;
			SendOrder(CUT_PAPER);
			return TRUE;
		}
		gettimeofday(&tv2,NULL);
		if (abs( (tv2.tv_sec % 10000) *1000+tv2.tv_usec/1000 - overtime) > 100)
			break;
	}
	return TRUE;	
}


/*--------------------------------------------------------------------------*
@Function			:PrintData - print data
@Include			: "print.h"
@Description			: buf: data that will be print
@Return Value		: Success TREU, Failure FALSE.
@Create time			: 2009-06-15 08:23		
*---------------------------------------------------------------------------*/

int PrintData(char *buf)
{
	int len,i=0;
	unsigned char *ptr;
	
	if(printdevinfo.printflag==0)	return FALSE;
	ptr=(unsigned char*)buf;
	if(ReadComStatus()==FALSE||printmemory.flag != 0||ptr==NULL) 
	{
		return FALSE;	
	}
	len=strlen(buf);
	do{
		printf("ptr=%s\n",ptr);
		if(ReadComStatus()==FALSE)	return FALSE;
		WriteComm( ptr, 50);
		i+=50;
		ptr+=i;
		usleep(100);
	}while(i<len);
	SendOrder(PRINT_ENTER);
	SendOrder(CUT_PAPER);
	return TRUE;	
}



/*--------------------------------------------------------------------------*
@Function		: Readstatus - read status of printer (1DH 72H n)
@Include		: "print.h"
@Description		: tranfer printer's status realtime
			   n=1,transfer printer status:
			bit	status	hex		function
			0	0	00		fix to 0
			1	1	02		fix to 1
			2	0/1	00/04	have/no cashbox open
			3	0/1	00/08	serial port busy/not
			4	1	10		tix to 1
			5				undefine
			6	0/1	00/40	printer ok/error
			7	0	0		fix to 0
			
@Return Value	: Success TREU, Failure FALSE.
@Create time		: 2009-06-15 08:23		
*---------------------------------------------------------------------------*/

int Readstatus()
{
	unsigned char str;
	int len=0;

	SendOrder(PRINT_STATUS);
//while(1)
{
	len=ReadComm(&str,1);
	if(len == 1)
	{
		printf("str=%02X %d\n",str,8&(int)str);	
		if(8&(int )str)
			return FALSE;	//buzy	
	}
}
	return TRUE;
}

//get printer's status  TRUE -not busy  FALSE-busy
int ReadComStatus()
{
	int bytes, status;

	ioctl(printdevinfo.fd, FIONREAD, &bytes);
	ioctl(printdevinfo.fd, TIOCMGET, &status);
	printf(" %d\n", status & TIOCM_CTS);
	if(status & TIOCM_CTS)
			return TRUE;
	return FALSE;
}

int SendOrder(char *command)
{
	int len=0,flag=0;
	unsigned char  buf[126];  
	
	if(command == NULL)		return FALSE;
	memset(buf,0,sizeof(buf));
printf("%s\n",command);
	len=gsmString2Bytes(command,buf,strlen(command));

	flag=WriteComm( buf, len);
	if(flag == 1)	
		return TRUE;
	else
		return FALSE;
}
