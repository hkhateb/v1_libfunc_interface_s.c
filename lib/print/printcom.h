#ifndef PRINTCOM_H__
#define PRINTCOM_H__
#include "../_precomp.h"

extern int fspfd;        //serial port
struct printdevinfo
{
	int fd;
	int printflag;
};
extern struct printdevinfo printdevinfo;
int UnPrintCom(void);
void PrintClear(void);
int OpenDev(int comport,int speed,int databits,int stopbits,int parity );
int set_speed(char *dev, int speed);
int set_Parity(int fd,int databits,int stopbits,int parity);
int WriteComm( unsigned char * data, int datalength) ;
int ReadComm (unsigned char *buf,int len);

#endif // !defined(COMM_H__)
