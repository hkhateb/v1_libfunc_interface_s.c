#include "printcom.h"

struct printdevinfo printdevinfo;




//set baudrate of printer
int set_printspeed(int speed)
{
	unsigned char opensetbuf[126]={0X1d, 0X28, 0X45, 0X03, 0X00, 0X01, 0X49, 0X4E};
	unsigned char closesetbuf[126]={0X1d, 0X28, 0X45, 0X04, 0X00, 0X02, 0X4f, 0X55, 0X54};
	char buf[126]={0X1D, 0X28, 0X45, 0X07, 0X00, 0X0B, 0X01};

	sprintf(&buf[7],"%d",speed);
	WriteComm(opensetbuf,8);	// turn to printer function setting mode   function 1
	WriteComm((unsigned char*)buf,30);
	WriteComm(closesetbuf,9);	//  end printer function setting mode   function 2
	return TRUE;
}
/*--------------------------------------------------------------------------*
@Function		: set_speed - set baudrate...
@Include      	: "comm.h"
@Description		:
			fd��file operation handler of opened file
			speed �� com baudrate
@Return Value	: Success TREU, Failure FALSE.
@Create time		: 2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int set_speed(char *dev, int speed){

	int  status,i;
	struct termios  Opt;
	int speed_arr[] = { B38400, B19200, B9600, B4800, B2400 };
	int name_arr[] = {38400,  19200,  9600,  4800,  2400 };

	printdevinfo.fd = open( dev, O_RDWR|O_NOCTTY|O_NDELAY);        //| O_NOCTTY | O_NDELAY
	if (printdevinfo.fd == -1)
	{
		perror("Can't Open Serial Port");
		return FALSE;
	}

	tcgetattr(printdevinfo.fd, &Opt);
	for ( i= 0;  i < sizeof(speed_arr) / sizeof(int);  i++) 
	{
		if  (speed == name_arr[i]) 
		{   
			tcflush(printdevinfo.fd, TCIOFLUSH);   
			cfsetispeed(&Opt, speed_arr[i]); 
			cfsetospeed(&Opt, speed_arr[i]); 
			status = tcsetattr(printdevinfo.fd, TCSANOW, &Opt); 
			if  (status != 0) 
			{       
				perror("tcsetattr fd1"); 
				return	  FALSE;   
			}   
			tcflush(printdevinfo.fd,TCIOFLUSH); 
		} 
	}
	return TRUE;
}

//adaptive baudrate
int atoi_speed(char *dev, int speed){
	int speed_arr[] = { B38400, B19200, B9600, B4800, B2400 };
	int name_arr[] = {38400,  19200,  9600,  4800,  2400 };
	int i=0;
	struct termios Opt;
	fd_set read_fd;
	unsigned char tmp[126]={0X10,0X04,0X01};
	struct timeval over_timer;

	for ( i= 0;  i < sizeof(speed_arr) / sizeof(int);  i++) 
	{
		printdevinfo.fd = open( dev, O_RDWR|O_NOCTTY|O_NDELAY);        //| O_NOCTTY | O_NDELAY
		if (printdevinfo.fd == -1)
		{
			perror("Can't Open Serial Port");
			return FALSE;
		}		
		tcgetattr(printdevinfo.fd, &Opt);
		tcflush(printdevinfo.fd, TCIOFLUSH);   
		cfsetispeed(&Opt, speed_arr[i]); 
		cfsetospeed(&Opt, speed_arr[i]); 
//		options.c_cflag |= (CLOCAL | CREAD);
		tcsetattr(printdevinfo.fd, TCSANOW, &Opt); 
		tcflush(printdevinfo.fd,TCIOFLUSH); 
		WriteComm(tmp,3);
		FD_ZERO(&read_fd);
		FD_SET(printdevinfo.fd,&read_fd);
		over_timer.tv_sec=0;
		over_timer.tv_usec=100000*((12-i)/2);//delay time,change with the baud rate 
		if((select(printdevinfo.fd+1,&read_fd,NULL, NULL, &over_timer)) <= 0)
		{
			close(printdevinfo.fd);	
			continue;
		}
		if(speed == name_arr[i])	return TRUE;
printf("2222\n");
		set_printspeed(speed);
		break;
	}
	close(printdevinfo.fd);
	sleep(2);
printf("1111\n");
	set_speed(dev,speed);
	return TRUE;
}

/**
  set serial port baudrate,databits��stopbits,paritybit
  fd    :file handler
  databits :7 or 8
  stopbits :1 or 2
  parity  :N,E,O,,S
*/
int set_Parity(int fd,int databits,int stopbits,int parity)
{
	struct termios options;

	if  ( tcgetattr( fd,&options)  !=  0) 
	{
		perror("SetupSerial 1");   
		return  FALSE; 
	}
	options.c_cflag &= ~CSIZE;
	switch (databits) /*set databits*/
	{ 
		case 7:
			options.c_cflag |= CS7;
			break;
		case 8:   
			options.c_cflag |= CS8;
			break; 
		default:   
			fprintf(stderr,"Unsupported data size\n"); 
			return FALSE; 
	}
	switch (parity)
	{ 
		case 'n':
		case 'N':   
			options.c_cflag &= ~PARENB;  /* Clear parity enable */
			options.c_iflag &= ~INPCK;    /* Enable parity checking */
			break; 
		case 'o': 
		case 'O':   
			options.c_cflag |= (PARODD | PARENB); /* set odd check*/ 
			options.c_iflag |= INPCK;            /* Disnable parity checking */
			break; 
		case 'e': 
		case 'E': 
			options.c_cflag |= PARENB;    /* Enable parity */   
			options.c_cflag &= ~PARODD;  /* convert to even*/   
			options.c_iflag |= INPCK;      /* Disnable parity checking */
			break;
		case 'S':
		case 's':  /*as no parity*/ 
			options.c_cflag &= ~PARENB;
			options.c_cflag &= ~CSTOPB;
			break; 
		default: 
			fprintf(stderr,"Unsupported parity\n");   
			return FALSE; 
	} 

	/* set stopbits*/ 
	switch (stopbits)
	{ 
		case 1:   
			options.c_cflag &= ~CSTOPB; 
			break; 
		case 2:   
			options.c_cflag |= CSTOPB; 
			break;
		default:   
			fprintf(stderr,"Unsupported stop bits\n"); 
			return FALSE;
	}
//	options.c_cflag |= CNEW_RTSCTS; /* you can call it CRTSCTS */
	/* Set input parity option */
	if (parity != 'n') 
		options.c_iflag |= INPCK;
	tcflush(fd,TCIFLUSH);
	options.c_cc[VTIME] = 150; /* overtime 15 seconds*/ 
	options.c_cc[VMIN] = 0; /* Update the options and do it NOW */
	if (tcsetattr(fd,TCSANOW,&options) != 0) 
	{
		perror("SetupSerial 3"); 
		return FALSE; 
	}
	return TRUE; 
}

/*
* 'open_port()' - Open serial port 1.
*
* Returns the file descriptor on success or -1 on error.
*/
int OpenDev(int comport,int speed,int databits,int stopbits,int parity )
{
	char Dev[126];
	
	memset(Dev,0,sizeof(Dev));
	sprintf(Dev,"/dev/ttyS%d",comport);

	atoi_speed(Dev,speed);
	if (set_Parity(printdevinfo.fd, databits, stopbits,parity) == FALSE)  {
		printf("Set Parity Error\n");
		return FALSE;
	}

	return TRUE;
}

/*--------------------------------------------------------------------------*
@Function			:	UnPrintCom - release resource
@Include      		: "comm.h"
@Description			: None
@param				: void
@Return Value			: Success TREU, Failure FALSE.
@Create time			: 2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int UnPrintCom(void)
{
	if(printdevinfo.fd>0)
		 close(printdevinfo.fd);
	else
		return FALSE;
	printdevinfo.fd=-1;
	return TRUE;
}

//clear com0 buffer
void PrintClear(void)
{
	if(printdevinfo.fd >0)
		tcflush(printdevinfo.fd,TCIOFLUSH);
}

/*--------------------------------------------------------------------------*
@Function					:	WriteComm - write data to com
@Include      		: "comm.h"
@Description			: None
@param				: data: data will be written
						  datalength: length of data to be written	
@Return Value			: Success  the number of bytes been written
@Create time			: 2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int WriteComm(unsigned char * buf, int len) {

	struct timeval over_timer;
	fd_set write_fd;
	unsigned char *ptr;
	int num=0,count=0;

	if(printdevinfo.fd<=0)		return FALSE;
	FD_ZERO(&write_fd);
	FD_SET(printdevinfo.fd,&write_fd);
	over_timer.tv_sec=0;
	over_timer.tv_usec=1250000;
	while(len!=num)
	{
		over_timer.tv_sec=0;
		over_timer.tv_usec=150000;
		if(select(printdevinfo.fd+1,NULL,&write_fd, NULL, &over_timer) <= 0)
		{
		    return num;
		}
		ptr=buf+num;
		count=write(printdevinfo.fd,ptr,len-num);
		if(count<=0)		return num;
		num+=count;
	}
	return num;
}

/*--------------------------------------------------------------------------*
@Function					:	ReadComm - read data form com
@Include      		: "comm.h"
@Description			: None
@param				: data: data will be read
						  datalength: length of data to be read
@Return Value			: Success  the number of bytes read; Failure FALSE
@Create time			: 2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int ReadComm (unsigned char *buf,int len)
{
	struct timeval over_timer;
	fd_set read_fd;
	unsigned char *ptr;
	int num=0;
	
	if(printdevinfo.fd<=0)	return FALSE;
	FD_ZERO(&read_fd);
	FD_SET(printdevinfo.fd,&read_fd);
	over_timer.tv_sec=0;
	over_timer.tv_usec=15000;
	while(len!=num)
	{
		over_timer.tv_sec=0;
		over_timer.tv_usec=100000;
		if(select(printdevinfo.fd+1,&read_fd,NULL, NULL, &over_timer) <= 0)
		{
		    return num;
		}
		ptr=buf+num;
		num+=read(printdevinfo.fd,ptr,len-num);
	}
	return num;
}



