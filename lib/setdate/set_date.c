#include "set_date.h"


/*--------------------------------------------------------------------------*
@Function			: MenuSetDate - set date
@Include      		: "readcard.h"
@Description			: sdate：date value format:2009-06-05
				opt : protocol
				RTC_SET - set rtc date
				RTC_NOTSET - not set rtc date
@Return Value		:
				Success TREU
				Failure FALSE
				Date format Error ERR_FORMAT		
*---------------------------------------------------------------------------*/
int MenuSetDate(IN const char *str,int opt)
{
	struct timeval tv;                                //时间结构
	struct timezone tz;                               //时区结构
	char cmdbuf[128];
	struct tm *tdate;
	time_t curdate;
	int year=0,mon=0,day=0,i=0,j=0;

	if(!str||strlen(str)!=10)
	{
		return ERR_FORMAT;
	}
	sscanf((char*)str,"%04d-%02d-%02d",&year,&mon,&day);          //截取参数日期
	if(year<2000||year>2050||mon<1||mon>12||day<1||day>31)
	{
		return ERR_FORMAT;
	}
	time (&curdate);                     
	tdate = localtime (&curdate);                    //获取当前时间
	tdate->tm_year=year-1900;
	tdate->tm_mon=mon-1;
	tdate->tm_mday=day;
	gettimeofday (&tv , &tz);                                //获取本机的时区信息
	tv.tv_sec=mktime(tdate);                                    ;//设置系统时间
	i=settimeofday(&tv,&tz);
	if(opt){
		sprintf(cmdbuf, "setdate -w %04d.%02d.%02d-%02d:%02d:%02d", year, mon,
		  day, tdate->tm_hour, tdate->tm_min, tdate->tm_sec); 
		j=system(cmdbuf);
	}
	if(i<0||(j<0&&opt))
	{
		return FALSE;
	}

	return TRUE;
}

/*--------------------------------------------------------------------------*
@Function			: MenuSetTime - set time
@Include      		: "readcard.h"
@Description			: sdate：date value（09:06:05）
				opt : protocol
				1 - set rtc time
				0 - not set rtc time
@Return Value		:
				Success TREU
				Failure FALSE
				Date format Error ERR_FORMAT		
*---------------------------------------------------------------------------*/
int MenuSetTime(IN const char *str,IN int opt)
{
	char cmdbuf[128];
	struct tm *tdate;
	time_t curdate;
	int hour,min,sec,i=0,j=0;
	struct timeval tv;                                //时间结构
	struct timezone tz;                               //时区结构

	if(!str||strlen(str)!=8)
	{
		return ERR_FORMAT;
	}
	sscanf((char*)str,"%02d:%02d:%02d",&hour,&min,&sec);      //截取参数日期
	if((hour>=24)||(hour<0)||(min>=60)||(min<0)||(sec>=60)||(sec<0))
	{
		return ERR_FORMAT;
	}
  
	time (&curdate);                     
	tdate = localtime (&curdate);                    //获取当前时间
	tdate->tm_hour=hour;
	tdate->tm_min=min;
	tdate->tm_sec=sec;
	gettimeofday (&tv , &tz);                                //获取本机的时区信息
	tv.tv_sec=mktime(tdate);                                    ;//设置系统时间
	i=settimeofday(&tv,&tz);
	if(opt)
	{
		sprintf(cmdbuf, "setdate -w %04d.%02d.%02d-%02d:%02d:%02d", tdate->tm_year+1900, tdate->tm_mon+1,tdate->tm_mday, hour, min,sec);
		j=system(cmdbuf);
	}

	if((i<0)||(j<0&&opt))
	{
		return FALSE;
	}

	return TRUE;
}

/*--------------------------------------------------------------------------*
@Function			: get_date - get date/time
@Include      		: "readcard.h"
@Description			: str：return date/time（2009-06-05 08:06:02）
				opt : protocol
				1 - read rtc date/time
				0 - do not read rtc date/time
@Return Value		: Success TREU, Failure FALSE.
@Create time			: 2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int GetDate(OUT char *str,IN int opt)
{
	struct tm *tm, p;
	time_t timep;

	if(opt == RTC_DISABLE)
	{	
		time(&timep);
		tm=localtime(&timep);
	  	sprintf(str,"%04d-%02d-%02d %01d %02d:%02d:%02d",
				tm->tm_year+1900,tm->tm_mon+1,tm->tm_mday,
				tm->tm_wday,tm->tm_hour, tm->tm_min, tm->tm_sec);
	}
	else if(opt == RTC_ENABLE)
	{
		if(read_rtc(&p) == FALSE)
		{
			perror("read rtc error\n");	
			return FALSE;
		}
	  	sprintf(str,"%04d-%02d-%02d %02d:%02d:%02d",
				p.tm_year+1900,p.tm_mon+1,p.tm_mday,
				 p.tm_hour, p.tm_min, p.tm_sec);
	}
	return TRUE; 
}

/*--------------------------------------------------------------------------*
@Function			: write_rtc - write rtc time
@Include      		: "readcard.h"
@Description			: ptr: pointer of struct tm
				this function is no longer in use
@Return Value		: Success TREU, Failure FALSE.
@Create time			: 2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int write_rtc(struct tm *ptr)
{
	struct rtc_time tm;
	int rtc_fd;

	rtc_fd = open("/dev/misc/rtc",O_RDWR);
	if (rtc_fd>0)
	{
		if(ptr)
			memcpy(&tm,ptr,sizeof(tm));
		if(ioctl(rtc_fd,RTC_ALM_SET,&tm)<0)
		{
			close(rtc_fd);
			return FALSE;
		}
		return TRUE;
	}
	return FALSE;
}

/*--------------------------------------------------------------------------*
@Function			: read_rtc - read rtc time
@Include      		: "readcard.h"
@Description			: ptr: pointer of struct tm
@Return Value		: Success TREU, Failure FALSE.
@Create time			: 2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int  read_rtc(struct tm *ptr)
{
	struct rtc_time tm;
	int rtc_fd;

	rtc_fd = open("/dev/misc/rtc",O_RDWR);
	if (rtc_fd>0)
	{
		ioctl(rtc_fd,RTC_RD_TIME,&tm);
		close(rtc_fd);
		if(ptr)
		{
			memcpy(ptr,&tm,sizeof(tm));
		}
		return TRUE;
	}
	return FALSE;
}

/*--------------------------------------------------------------------------*
@Function			:syncclock - The clock efficacy
@Include      		: "readcard.h"
@Description			: clock_tz: calibration value
@Return Value		: Success TREU, Failure FALSE.
@Create time			: 2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int Sysclock(IN int clock_tz)
{
        int fd;
        //open device file
        fd = open("/dev/CLK_ADJUST",O_RDWR);
        if(fd < 0)
        {
                perror("device open fail\n");
                return FALSE;
        }

         if(ioctl(fd,WRITESJ,&clock_tz)!=0)
          {
           perror("writesj error\n");
		return FALSE;
          }
	close(fd);
        return TRUE;
}

//int main()
//{
//	set_date(NULL, "08:02:03",1);
//}
