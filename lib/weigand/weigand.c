#include "weigand.h"
#include "../card/readcard.h"

int wgreadfd=0,wgoutfd=0;
 
//Initialize WG read
int InitWiganRead(void)
{
	if((wgreadfd=open("/dev/wg_drv",O_RDWR))==-1)
	{
		perror("open wg_drv error");
   		return FALSE;
	}
	//printf("initWiganread success\n");
	return TRUE;
}

//Hight-Low bits transform
// 10000100->00100001
struct mem_inout data_covert(struct mem_inout smem,size_t len)
{
struct mem_inout tmp;
 int i;
 unsigned int j,k,c;
memset(&tmp,0,sizeof(tmp));
if(len>sizeof(tmp)*8)return tmp;

for(i=len-1;i>=0;i--)
{
j=i/(sizeof(int)*8);
k=i%(sizeof(int)*8);
c=1<<k;
if(c&smem.d[j]){
        j=(len-i-1)/(sizeof(int)*8);
        k=(len-i-1)%(sizeof(int)*8);
        c=1<<k;
        tmp.d[j]=tmp.d[j]|c;
        }
//else flag=0;
}
return tmp;
}

//Read WG data
int ReadWigan(int swg,struct mem_inout*wg_data)
{ 
	struct mem_inout smem;

	if(wgreadfd<=0)	return 0;
	//read(wgreadfd,(unsigned char *)&wg_data,4);
	memset(&smem,0,sizeof(smem));
 if(ioctl(wgreadfd,swg,&smem)!=0)
 {
  perror("wg error\n");
  return -1;
 }
//printf("read success 1 %X\n",smem.d[0]);
 //Read Success
 if(smem.d[2]&&0X80000000)
smem=data_covert(smem,swg);
else return -1;
//printf("read success 2\n");
*wg_data=smem;
return 0;
}

//Close WG read
int CloseWiganRead(void)
{
  if(wgreadfd <=0 )	return FALSE;
  if(close(wgreadfd) ==-1 ) return FALSE;
  wgreadfd = -1;
  return TRUE;
}

//Initialize WG out
int InitWiganOut(IN char *path)
{
	if(wgoutfd=open(path,O_RDWR)==-1)
        {
	   return FALSE;
        }

	return TRUE;
}

//WG output
int SendDataWigan(char *buf)
{
	unsigned int data=0;

	if(wgoutfd ==-1 || buf ==NULL )	return FALSE;
	data=atoi(buf);
	ioctl(wgoutfd,WGOUT26,&data);
	return TRUE;
}

//Close WG output
int  CloseWiganOut(void)
{
  if(wgoutfd != -1)
	if(close(wgoutfd) ==-1 ) return FALSE;
  wgoutfd = -1;
  return TRUE;
} 

//Enable output
int EnableOutPut()
{
	return TRUE;
}

//Close output
int CloseOut()
{
	return TRUE;
}

//Set pulse
int SetPulse(int msec)
{
	return TRUE;
}

//Standby mode
int StandbyMode()
{
	return TRUE;
}

//Power supply mode
int PowerSupply()
{
	return TRUE;
}

//Low power warning
int LowPowerWarning(int value)
{
	return TRUE;
}
