#ifndef __GPRS_H
#define __GPRS_H

#include "../public/public.h"
#include "../net/net_tcp.h"
#include "../_precomp.h"

 int InitGprs(int port,int rate,int overtime,int protocol,char *deputyip,int deputyport);
 int GprsAccept(unsigned char *data,int len);
 void CloseLink();
 int GprsRecv(unsigned char *data,int len);
 int GprsSend(unsigned char *data,unsigned int len);
 void GprsJianche();

int my_read(int fd,unsigned char *buf,int len);
int my_write(int fd,unsigned char *buf,int len);


int search_gprs_btl();
int _search_gprs_btl();
int open_link(int tongdao);
void gprs_apn();
void reg_apn();


int gprs_send(unsigned char *data,unsigned int len,int tongdao);
int gprs_tcp_send(unsigned char *data,unsigned int len);
int get_gprs_data(unsigned char *data);
int gprs_recv(unsigned char *data,int len,int tongdao);
int gprs_tcp_recv(unsigned char *data,int len);
int gprs_udp_send(unsigned char *data,unsigned int len);//channel 2
int gprs_video_send(unsigned char *data,unsigned int len);//channel 3
int gprs_udp_recv(unsigned char *data,int len);
int gprs_reset(void);
void gprs_config();
extern int gprs_reset_time;
void gprs_write_mac();
 void gprs_rssi();
int read_state(int tongdao);
void gprs_tsim();
int read_rssi();
void gprs_write_head();
extern int gprs_fd; 
void reset_send_time();
int ascii_2_hex(unsigned char  *O_data,unsigned char *N_data, int len);
int hex_2_ascii(unsigned char *data, unsigned char *buffer, int len);
#endif
