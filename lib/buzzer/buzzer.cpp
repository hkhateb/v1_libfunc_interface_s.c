#include "buzzer.h"


#define  SOUNDGPF2  0x0100

int  buzzer_fd=-1;


/*--------------------------------------------------------------------------*
@Function            	OpenBuzzer - Open buzzer device
@Include      	buzzer.h
@Description		path��path of buzzer devcie file
@Return Value		Success TRUE, Failure FALSE.
@Create time		2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int   OpenBuzzer(char *path)
{
	if((buzzer_fd=open(path,O_RDWR))==-1)
	{
                return  FALSE;
	}
	return TRUE;
}

/*--------------------------------------------------------------------------*
@Function            	BuzzerOn - buzzer on
@Include      	buzzer.h
@Description		
			buzzer sound
@Return Value		Success TRUE, Failure FALSE.
@Create time		2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int  BuzzerOn(void)
{
  int  w=0;
  if(buzzer_fd<=0)	return FALSE;
  ioctl(buzzer_fd,SOUNDGPF2,&w);
  return TRUE;
}

/*--------------------------------------------------------------------------*
@Function            	BuzzerOff - buzzer off
@Include      	buzzer.h
@Description		
			stop beeping
@Return Value		Success TRUE, Failure FALSE.
@Create time		2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int  BuzzerOff(void)
{
  int  w=1;
  if(buzzer_fd<=0)	return FALSE;
  ioctl(buzzer_fd,SOUNDGPF2,&w);
   return TRUE;	
}

/*--------------------------------------------------------------------------*
@Function            	CloseBuzzer - Close buzzer device
@Include      	buzzer.h
@Description		
			release resource
@Return Value		Success TRUE, Failure FALSE.
@Create time		2009-06-15 08:23		
*---------------------------------------------------------------------------*/
int  CloseBuzzer(void) {

  if(buzzer_fd != -1)
	if(close(buzzer_fd) ==-1 ) return FALSE;
  buzzer_fd = -1;
  return TRUE;
} 
