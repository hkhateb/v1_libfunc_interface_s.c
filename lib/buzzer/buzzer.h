#ifndef C_SOUND_H
#define C_SOUND_H

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include "../_precomp.h"

extern int   OpenBuzzer(char *path);
extern int   BuzzerOn(void);
extern int   BuzzerOff(void);
extern int   CloseBuzzer(void);

#endif
