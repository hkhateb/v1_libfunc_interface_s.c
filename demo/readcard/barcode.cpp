#include <stdio.h>  
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "../libfunc.h"


int main(int argc, char *argv[]){

	char buf[128],cardsno[128];
	char cardtype;
	COMINFO cominfo[3];
	struct card_INFO cardinfo;

	if(argc<2||atoi(argv[1])>6)	
	{
		printf("pls input baud rate\n");
		return -1;
	}
	cominfo[0].enable=0;
	cominfo[2].enable=0;
	cominfo[1].enable=1;
	cominfo[1].baudrate=atoi(argv[1]);	//2400,4800,9600,19200,38400,57600,115200
	cominfo[1].workmode=1;
	
	cardinfo.track=2;

	if(OpenCom(cominfo) == FALSE) // Initialize serial port
	{
		perror("com init error");
		return -1;
	}

	InitCard(&cardinfo);
	printf("pls put  card \n");

	while(1)
	{
		if(ReadCom1()==FALSE)	continue;	//
		memset(buf,0,sizeof(buf));
		cardtype=ReadCard(buf);
//		if(cardtype==0X00)	continue;
		printf("buf:%s\n",buf);

		if(cardtype==READBARCODE)
			printf("READBARCODE %s\n",buf);	
	}

	UnCom(1);// Close serial device
	return 0;
}
