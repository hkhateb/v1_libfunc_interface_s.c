#include <stdio.h>  
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "../libfunc.h"


int main(int argc, char *argv[]){

	char buf[128],cardsno[128],*Pwg=NULL;
	int cardtype,wgtype=0,tmp=0,i=0,len=0;
	unsigned long  int data=0;
	COMINFO cominfo[3];
	struct card_INFO cardinfo;

	if(argc<2||atoi(argv[1])>6)	
	{
		printf("pls input baud rate\n");
		return -1;
	}
	cominfo[0].enable=0;
	cominfo[2].enable=0;
	cominfo[1].enable=1;
	cominfo[1].baudrate=atoi(argv[1]);	//2400,4800,9600,19200,38400,57600,115200
	cominfo[1].workmode=1;
	
	cardinfo.track=2;

	if(OpenCom(cominfo) == FALSE) // Initialize serial port
	{
		perror("com init error");
		return -1;
	}

	InitCard(&cardinfo);
	printf("pls put  card \n");

	while(1)
	{
		if(ReadCom1()==FALSE)	continue;	//
		memset(buf,0,sizeof(buf));
		cardtype=ReadCard(buf);
//		if(cardtype==0X00)	continue;
		//printf("buf:%s\n",buf);
		if(cardtype!=0)
		printf("return value: %d\n",cardtype);
		switch(cardtype)
		{
			case -1:printf("read magetic failure\n");break;
			case -2:printf("read barcode decode failure\n");break;
			case -3:printf("read barcode failure\n");break;
			case 0X9C: //HID  
					len=buf[0];
					wgtype=buf[1];
					Pwg=buf+2;
					printf("wgtype %d;len=%d\n",wgtype,len);
					switch(wgtype)
					{
					case 1:	//wg26 26bit; 4 byte	
						data=0;
						for(i=0;i<len-1;i++){
							data=(data<<8)|Pwg[i];
							printf("%X,%X\n",data,Pwg[i]);
						}
						data=(data&0x1FFFFFE)>>1;//get wg valid bit ;(clear odd bit/even bit) you can calc these
						//your card no 33295/33294  by data&0xffff mode 
						printf("HID data:%lud,%03ld,%05ld\n",data,(data&0XFF0000)>>16,data&0xffff);
						break;
					case 2://wg34	
						break;
					}
			case 0x97://voltage
			case 0x98://power mode
			case 0:break;
			default:printf("other card :%s\n",buf);
		}
		if(cardtype==0X93){
			memset(cardsno,0,sizeof(cardsno));
			card_linearhandle(buf,cardsno);
			printf("card no.:%s\n",cardsno);
			memset(cardsno,0,sizeof(cardsno));
			card_synelhandle(buf,cardsno);
			printf("card no.:%s\n",cardsno);
		}
	}

	UnCom(1);// Close serial device
	return 0;
}
