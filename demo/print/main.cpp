#include<stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include "../libfunc.h"

int main(int argc, char *argv[])
{
	COMINFO cominfo[3];

	cominfo[0].enable=1;	//serial 0 enable
	cominfo[2].enable=0;	//serial 2 disable
	cominfo[1].enable=0;	//serial 1 disable
	cominfo[0].baudrate=2;	//set serial 0 baud rate 4800
	cominfo[0].workmode=1;	

	if(OpenCom(cominfo) == FALSE) // Initialize serial port
	{
		perror("com init error");
		return -1;
	}


	PrintData("this test section!!");   //print data

	UnPrintCom();
	return 0;
}
