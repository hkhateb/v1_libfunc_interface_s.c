#include <stdio.h>  
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "../libfunc.h"

int main(int argc, char *argv[]){
	COMINFO cominfo[3];

	cominfo[0].enable=0;
	cominfo[2].enable=0;
	cominfo[1].enable=1;
	cominfo[1].baudrate=6;
	cominfo[1].workmode=1;

	if(OpenCom(cominfo) == FALSE) // Initialize serial port
	{
		perror("com init error");
		return -1;
	}
	Set_Machine_Mode(0);

	UnCom(1);// Close serial device
	return 0;
}
