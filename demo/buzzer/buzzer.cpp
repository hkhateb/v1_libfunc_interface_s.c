#include<stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include "../libfunc.h"

int main(int argc, char *argv[])
{
  int i=0;

  if(OpenBuzzer("/dev/GSM") == FALSE) // open buzzer device
   {		
	perror("open buzzer error");
	return -1;
   }

  while(i<10){
	if( BuzzerOn() == FALSE) // set buzzer on
	{
		perror("sound buzzer error");
		break;
	}
	usleep(120000);
	i++;
  }

  if(BuzzerOff()==FALSE) // set buzzer off
	{
		perror("close buzzer error");
		return -1;
	}

  if(CloseBuzzer()== FALSE) // Close buzzer device
	{
		perror("close keybord error");
		return -1;
	}
  return 0;
}
