#include <stdio.h>  
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "../libfunc.h"

//write card sectors
int write_sectors()
{
	int i=0,j=0;
	char buf[1024],cardsno[128];
	
	//read/write sectors
	memset(buf,0,sizeof(buf));
	for(i=0;i<=56;i++)
	{
		strncpy(&buf[j],"9876543210aaaaaa",16);
		j+=16;
	}
	i=0;

	while(1)
	{

		if(ReadCom1()==FALSE)	continue;	//
		memset(cardsno,0,sizeof(cardsno));
		if(ReadCard(cardsno)==0)	continue;

		if(MF1Write(cardsno,buf)==TRUE)
			printf("write ok\n");
		else
			printf("write Error\n");
		break;
	}
	return 0;
}
//read card sectors
int read_sectors()
{
	int i,len=0;
	char buf[1024],cardsno[128];

	while(1)
	{
		if(ReadCom1()==FALSE)	continue;	//
		memset(cardsno,0,sizeof(cardsno));
		if(ReadCard(cardsno)==0)	continue;

		len=MF1Read(cardsno,buf);
		if(len==-1)
		{
			printf("read error\n");
			return -1;
		}
		for(i=0;i<len;i++)
		{
			if(i!=0&&i%16==0)	printf("\n");
			printf("%c ",buf[i]);
		}
	}
	printf("\nread ok\n");
	//read/write sectors end
	return 0;
}

int main(int argc, char *argv[]){

	char buf[128]="FFFFFFFFFFFF";
	
	COMINFO cominfo[3];
	struct card_INFO cardinfo;
	struct R_card_INFO ReadCardInfo;
	struct W_card_INFO WriteCardInfo;

	if(argc<3||atoi(argv[1])>6)	
	{
		printf("pls input baud rate\n");
		return -1;
	}
	cominfo[0].enable=0;
	cominfo[2].enable=0;
	cominfo[1].enable=1;
	cominfo[1].baudrate=atoi(argv[1]);	//2400,4800,9600,19200,38400,57600,115200
	cominfo[1].workmode=1;

	//read sectors
	ReadCardInfo.beg_sectors=4;
	ReadCardInfo.end_sectors=4;
	ReadCardInfo.beg_block=0;
	ReadCardInfo.end_block=2;
	ReadCardInfo.enable=1;		//read section enable;
	strcpy(ReadCardInfo.Akey,buf);
	strcpy(ReadCardInfo.Bkey,buf);

	//write sectors
	WriteCardInfo.beg_sectors=2;
	WriteCardInfo.end_sectors=2;
	WriteCardInfo.beg_block=0;
	WriteCardInfo.end_block=0;
	WriteCardInfo.enable=1;		//write section enable;
	strcpy(WriteCardInfo.Akey,buf);
	strcpy(WriteCardInfo.Bkey,buf);

	if(OpenCom(cominfo) == FALSE) // Initialize serial port
	{
		perror("com init error");
		return -1;
	}
	InitCard(&cardinfo);
	InitCard_Read(&ReadCardInfo);
	InitCard_Write(&WriteCardInfo);

	printf("pls put mf1 card \n");
	if(atoi(argv[2])==0)
		read_sectors();
	else
		write_sectors();
	UnCom(1);// Close serial device
	return 0;
}
