#include<stdio.h>
#include<stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include "../libfunc.h"

int netback(const char* filename )
{
	printf("filename=%s\n",filename);
	return 0;
}
int endback( int wddaflag )
{
	MenuKey("ft");
	printf("wddaflag=%d\n",wddaflag);
	return 0;
}

int verify_manger(char *id,char *password,char *card)
{
	printf("manger=%s,%s,%s\n",id,password,card);
	return 0;
}

int main(int argc, char *argv[])
{
	int flag=1;
	COMINFO cominfo[3];

	if(argc<2)	
	{
		printf("pls input baud rate\n");
		return -1;
	}
	cominfo[0].enable=0;
	cominfo[1].enable=0;
	cominfo[2].enable=1;
	cominfo[2].baudrate=atoi(argv[1]);
	cominfo[2].workmode=0;
	cominfo[2].overtime=30;
	CALBACK calback;
	NETINFO netinfo;


	calback.netback=netback;
	calback.endback=endback;
	calback.verify_manger=verify_manger;

	strcpy(netinfo.netovertime,"30");
	strcpy(netinfo.netport,"3350");
	strcpy(netinfo.netserverip,"192.168.0.95");
	netinfo.linetype=1;
	netinfo.line=0;
	if(OpenGpioBoard("/dev/GPIO2_5") == FALSE) // Open GPIO device and initialize it 
	{
		return -1;
	}
	SOUNDINFO soundinfo;

//	soundinfo.maxyl=80;
	soundinfo.sound_kq=1;
	InitVolume(&soundinfo);

	if(InitNet(&netinfo,&calback)==FALSE)
		printf("Init error\n");

	if(OpenCom(cominfo) == FALSE) // Initialize serial port
	{
		perror("com init error");
		return -1;
	}
	while(1)
	{
		flag=AcceptLinecom();
		if(flag ==1) 	//judge whether there is connection requestion
		{ 
			MenuKey("fw");
			printf("com line ok\n");	
		}	
		else if(flag==2)
		{
			ComSend();
		}
	}
	UnCom(2);
	return 0;
}
