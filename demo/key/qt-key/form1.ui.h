#include "form1.h"
#include "main.h"
#include<stdio.h>
#include <qnamespace.h>
#include <qevent.h>
#include <qwidget.h>
#include <qwidgetstack.h>
#include <qapplication.h>
#include <qdialog.h>
#include <qiodevice.h>
#include <qfile.h>
#include <qdir.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qimage.h>
#include <qpixmap.h>
#include "libfunc.h"

int itsTimer;

void Form1::init()
{   
  setGeometry(-5,-5,320,240);
  OpenKeyBoard("/dev/kbd_7289");		//
  itsTimer=startTimer(120);  //
 
}


//intput
void Form1::ShowString(int k)
{
  QString temp;
  temp.sprintf(input->text() + "%d", k);    
  input->setText(temp);
}
//Clear
void Form1::DeleString()
{
  QString str=input->text();
  uint k=str.length();
  if (k>0)  input->setText(str.left(k-1)); 
}



int key=-1;
void Form1::timerEvent( QTimerEvent * e)
{
  if (e->timerId() == itsTimer)
  {
	 key=ThreadKeyBoard(0);
	if(key!=-1)	{
		if(key==11)
			DeleString();
		else
			ShowString(key);
	    }

   }
}
