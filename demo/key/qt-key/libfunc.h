#ifndef LIBFUNC_H
#define LIBFUNC_H 

#define TRUE		1
#define FALSE	0

extern "C" {

//key
 int OpenKeyBoard(char *path);	
 int CloseKeyBoard(void);
 int ThreadKeyBoard(void);
 char *ReadKey(int overtime,int mask);

//camera
 int OpenCamera(char *path);
 int GetCameraImage(char* FileName);           
 int CameraClose(void);

//sound
 int setvolume(int percent);

//wigan
 int InitWiganOut(char *path);
 int InitWiganRead(char *path);
 
 int SendDataWigan(char *buf) ;
 unsigned char * ReadWigan(int opt);
 unsigned char * ReadWigan_Original(int opt);
 
 int CloseWiganOut(void);
 int CloseWiganRead(void);

//gpio
 int OpenGpioBoard(char *path);
 int GpioOpt(int opt);
 int LedFlashing(int ledno,char * bgtime,int duration);
 int CloseGpioBoard(void);

//card
 int InitCom(int port,int baudrate,int protocol);
 int UninitCom(void);
 int  ReadClear(void);
 unsigned char *ReadOnlyCard(void);
 unsigned char *ReadMagnetic(void);
 unsigned char *ReadBarcode(void);
 unsigned char *  ReadIC(int sectors,int begin,int len);
 int  WriteIC(int sectors,unsigned char *str);
 int LoadKey(char mode,char secnr,char *nkey);

//finger
 int  InitFp(char *lbpath,int nSensorType);
 int  LoadFpData(char *nID,int FingerNum,char *FileName);
 int  Enroll( char *nID ,int FingerNum,char *tpath,char *dpath ,int num);
 long OneToNMatch(char *tpath);
 long OneToOneMatch(char *nID,int FingerNum,char *tpath);
 int  DeleteFpOne(char *nID, int FingerNum);
 int  DeleteFpOneAll(char *nID);
 int  DeleteFpAll(void);
 unsigned char *GetFpDataOne(char *nID,int FingerNum);
 FILE * GetFpList();
 int setFpData(char *nID,int FingerNum,unsigned char *str);
 int UninitFp(void);

//pc-finger
 int InitFp_pc(char *lbpath, int nSensorType);
 long OneToNMatch_pc(long *nPos,char *tpath);
 int Enroll_pc(long nID,char *tpath) ;
 int LoadFpData_pc(long anpos,char *FileName);


//date
 int syncclock(int clock_tz);
 int set_date(const char *date, const char *time,int opt);
 int get_date(char *str,int opt);

//net line
int InitNet(char * localport,char *overtime,int nettype,int serverport,char *serverip);
 int AcceptLinezd();
 int Net();
 void UninitNet();

//com line
 int OpenCom(int port,int baudrate,int overtime);	
 int AcceptLinecom();
 int ComSend();  
 
//SD card  u
 FILE* creatdirfile(char *path);
 int safe_cp(char * oldpath,char *newpath);
 int safe_rm(char * oldpath);

//out
 int EnableOutPut();
 int CloseOut();
 int SetPulse(int msec);

//power
 int StandbyMode();
 int PowerSupply();
 int LowPowerWarning(int value);

//print
 int UnPrintCom(void);
 int OpenDev(char *Dev,int speed,int databits,int stopbits,int parity );
 int PrintFile(char *filename);
 int PrintData(char *buf);
 int InitPrint();

//Gprs
 int InitGprs(int port,int rate,int overtime,int protocol,char *deputyip,int deputyport);
 int GprsAccept(unsigned char *data,int len);
 void GprsJianche();
 void CloseLink();
 int GprsRecv(unsigned char *data,int len);
 int GprsSend(unsigned char *data,unsigned int len);

/*
 int InitGprsPort(int port,int rate,int protocol);
 int SelectLineMode(char *mode);
 int SendData(char *value);
 int ReceiveData(char *Value);
 int CloseGprsPort();
*/

//buzzer
 int   OpenBuzzer(char *path);
 int   BuzzerOn(void);
 int   BuzzerOff(void);
 int   CloseBuzzer(void);

}

#endif 
