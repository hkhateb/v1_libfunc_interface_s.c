/****************************************************************************
** Form implementation generated from reading ui file 'form1.ui'
**
** Created: Wed Jul 22 16:47:52 2009
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "form1.h"

#include <qvariant.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include "form1.ui.h"

/*
 *  Constructs a Form1 as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
Form1::Form1( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "Form1" );

    input = new QLabel( this, "input" );
    input->setGeometry( QRect( 110, 80, 150, 31 ) );
    input->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    QFont input_font(  input->font() );
    input_font.setPointSize( 16 );
    input->setFont( input_font ); 
    input->setFrameShape( QLabel::Panel );
    input->setFrameShadow( QLabel::Sunken );
    input->setLineWidth( 2 );
    languageChange();
    resize( QSize(600, 480).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
Form1::~Form1()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void Form1::languageChange()
{
    setCaption( tr( "Form1" ) );
    input->setText( QString::null );
}

