/****************************************************************************
** Form interface generated from reading ui file 'form1.ui'
**
** Created: Wed Jul 22 16:47:50 2009
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef FORM1_H
#define FORM1_H

#include <qvariant.h>
#include <qwidget.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;

class Form1 : public QWidget
{
    Q_OBJECT

public:
    Form1( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~Form1();

    QLabel* input;

public slots:
    virtual void ShowString( int k );
    virtual void DeleString();
    virtual void timerEvent( QTimerEvent * e );

protected:

protected slots:
    virtual void languageChange();

private:
    void init();

};

#endif // FORM1_H
