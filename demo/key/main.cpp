#include <stdio.h>  
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "../libfunc.h"

int main(int argc, char *argv[]){

	int key=-1,mode=-1;
	char *p=NULL;
	COMINFO cominfo[3];
	KEYINFO keyinfo;
	SOUNDINFO soundinfo;

	if(argc<3)	return -1;
	cominfo[0].enable=0;
	cominfo[2].enable=0;
	cominfo[1].enable=1;
	cominfo[1].baudrate=atoi(argv[1]);
	cominfo[1].workmode=1;

	keyinfo.enable=1;
	keyinfo.mode=atoi(argv[2]);
	keyinfo.timeout=2;

	soundinfo.sound_kq=1;
	InitVolume(&soundinfo);

	if(OpenCom(cominfo) == FALSE) // Initialize serial port
	{
		perror("com init error");
		return -1;
	}
	InitKeyBoard(&keyinfo);
	printf("pls key \n");
	while(1)
	{
		if(ReadCom1()==FALSE)	continue;
		key=ThreadKeyBoard();
		if(key==-1)	continue;
		printf("key=%d\n",key);	
		switch(key)
		{
			case 11:
				mode=GetKeyMode(1);
				printf("mode=%d\n",mode);
				break;
			case 12:
				SetKeyMode(1);
				break;
			default:
				p=ReadKey(key);
				printf("%s\n",p);
			break;
		}
	}

	UnCom(1);// Close serial device
	return 0;
}
