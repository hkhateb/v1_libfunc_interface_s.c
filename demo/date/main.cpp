#include<stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include "../libfunc.h"

int main(int argc, char *argv[])
{
  char buf[126];
  int error=0;
  
  if(argc<3)	return -1;
  if(GetDate(buf,RTC_ENABLE) == FALSE)		//get date/time of currect 
   {		
	perror("get date error");
	return -1;
   }
  printf("currect time=%s\n",buf);

  // set date 
  error=MenuSetDate(argv[1],RTC_ENABLE);
  if( error== FALSE)	
   {		
	perror("set date error");
   }
  else if(error==ERR_FORMAT)
	perror("date format error");

  printf("set date ok");
  // set time 
  error=MenuSetTime(argv[2],RTC_ENABLE);
  if( error== FALSE)	
   {		
	perror("set time error");
	return -1;
   }
  else if(error==ERR_FORMAT)
	perror("time format error");

  printf("set time ok");
  return 0;
}
