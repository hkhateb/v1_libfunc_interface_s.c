//when include dynamic library,parameter -ldl should be added to compiler
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include "../libfunc.h"

//load finger Template
int load_finger(char *fpath)
{
	DIR *db;
	char filename[64],*buf,tmp1[64],tmp2[64];
	struct dirent *p;
	int len;

	db=opendir(fpath);
	if(db==NULL)		return -1;
	memset(filename,0,64);
	while ((p=readdir(db)))
	 {  
	    if((strcmp(p->d_name,".")==0)||(strcmp(p->d_name,"..")==0))
      		 continue;
	    else
      	     {  
		 if(strstr(p->d_name,".s10")){
			buf=p->d_name;
		      len=strcspn(buf,"_");
			memset(tmp1,0,sizeof(tmp1));
			strncpy(tmp1,buf,len); 
			buf+=len;
			 buf++;
			len=strcspn(buf,".");
			memset(tmp2,0,sizeof(tmp2));
			strncpy(tmp2,buf,len); 
			sprintf(filename,"%s%s",fpath,p->d_name); 
			LoadFpData_pc(atol(tmp1)*10+atoi(tmp2),filename);
		 }
      	}
    	memset(filename,0,64);
 	 }
   closedir(db);
   return 0;
}


/*
return the owner's ID of the finger
*/
char * selectfinger()
{
	long anpos=0;
	long ret=0;
	static char xh[32];

	ret=OneToNMatch_pc(&anpos,"/dev/shm/zw.bmp");
	if(ret==0)
	{
		return NULL;
	}
	if(ret==1)
	{ 
		memset(xh,0,sizeof(xh));
		strcpy(xh,"");
		return xh;
	}
	memset(xh,0,sizeof(xh));
	sprintf(xh,"%ld",anpos/10);
	return xh;
}


int main(int argc, char *argv[])
{
	int i=0,nRet=0,fpnum=0;
	char buf[126],nID[126],anpos[126];
	unsigned char *str;
	char *zw;
	
	if(InitFp_pc("./lib/sfe.so", 1) == -1) // initialize fingerprint device
	{
		printf("init error\n");	
		return -1;
	}

	//Enroll fingerprint
	while(1){
		printf("input xu hao: \n");
		memset(nID,0,sizeof(nID));
		fgets(nID,sizeof(nID),stdin);
		printf("input finger number:\n");
		memset(buf,0,sizeof(buf));
		fgets(buf,sizeof(buf),stdin);
		fpnum = atoi(buf);
		Enroll_pc(atol(nID)*10+fpnum,"/dev/shm/zw.bmp");
		break;
	}
	while(1){
		zw = selectfinger();
		if(zw!=NULL)
		{
			printf("onetoN=%s\n",zw);
			break;
		}
	   }

	return 0;
}
