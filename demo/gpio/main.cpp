#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../libfunc.h"

int main(int argc, char *argv[])
{
	char buf[12];
	//init
	if(OpenGpioBoard("/dev/GPIO2_5") == FALSE) // Open GPIO device and initialize it 
	{
		return -1;
	}
	Gpio_Sub((const char*)"G,S,2");
	Gpio_Sub((const char*)"R,B,2");
	Gpio_Sub((const char*)"0,S,2");
/*
	GetIostate();
	while(1)
	{
		fgets(buf,sizeof(buf),stdin);
		if(GpioOpt(atoi(buf)) == FALSE) // Specific operation to GPIO
		{
			printf("error");
		}
	}
*/
	while(1)
	{
		LedFlashing(); // led on at time you specified
	}

	if(CloseGpioBoard() == FALSE) // Close GPIO device
	{
		return -1;
	}
	return 0;
}
